module Main where

import System.Environment (getArgs)
import System.IO (hFlush, stdout)

import Game.Spec

main :: IO ()
main = do args <- getArgs
          userInput <- getContents
          path <- case args of
                    [] -> do putStr "Give path to game file: "
                             hFlush stdout
                             return $ head $ words $ userInput
                    path : _ -> return path
          gameContext <- readGame path
          putStrLn $ show gameContext
