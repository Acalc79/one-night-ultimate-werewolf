module Main where

import System.Environment (getArgs)

import Control.Monad.Reader (runReaderT)

import Data.Core (GameState)
import Game.Spec (readGame)
import Game (runOneGame)

main :: IO GameState
main = do args <- getArgs
          path <- case args of
                    [] -> do putStrLn "Give path to game file:"
                             getLine
                    path : _ -> return path
          playerNames <- case args of
            _ : l@(_ : _) -> return l
            _             -> putStrLn "Give player names:" >> words <$> getLine
          gameContext <- readGame path
          runReaderT runOneGame (gameContext, playerNames)
