{
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
module Data.Parser
  ( ErrorType, isValidName
  , parseAction, parseCondition
  , parseSetup, parseRoleSpec, parseGameFile
  , parserToReaderFile, parserToReadFile, runParser, runParserT) where

import Control.Monad (join)
import Control.Monad.IO.Class (MonadIO, liftIO)
import qualified Data.Foldable as Fold
import Data.Maybe (mapMaybe)
import Numeric (readFloat)
import Data.Char (isSpace, isDigit, isAlpha, isAlphaNum)

import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map

import Control.Monad.Reader (MonadReader, ask, asks)
import Control.Monad.Except (throwError, catchError)

import System.FilePath ((</>), joinDrive)

import Control.Lens hiding (Context)

import Data.Core
import Data.Lenses
import Data.Parser.Core
import Data.Parser.Function
}
%name parseAction Action
%name parseCondition Condition
%name parsePlayerSet PlayerSet
%name parseNumber Number
%name parseSetup SetupFile
%name parseRoleSpec RoleFile
%name parseGameFile GameFile
%tokentype { Token }
%error { parseError }
%monad { ParserMonad }
%token
  '#' { TokenHash }
  '=' { TokenEq }
  '/' { TokenSlash }
  '/=' { TokenNeq }
  '<' { TokenLt }
  '>' { TokenGt }
  '<=' { TokenLeq }
  '>=' { TokenGeq }
  '(' { TokenOpenBracket }
  ')' { TokenCloseBracket }
  '-' { TokenHyphen }
  ':' { TokenColon }
  prompt { TokenName "prompt" }
  finish { TokenName "finish" }
  order { TokenName "order" }
  time { TokenName "time" }
  action { TokenName "action" }
  table { TokenName "extra-center" }
  setup { TokenName "setup" }
  death { TokenName "death" }
  default { TokenName "default" }
  any { TokenName "any" }
  died { TokenName "died" }
  in { TokenName "in" }
  play { TokenName "play" }
  -- saw { TokenName "saw" }
  most { TokenName "most" }
  voted { TokenName "voted" }
  not { TokenName "not" }
  and { TokenName "and" }
  or { TokenName "or" }
  for { TokenName "for" }
  of { TokenName "of" }
  self { TokenName "self" }
  left { TokenName "left" }
  right { TokenName "right" }
  adjacent { TokenName "adjacent" }
  others { TokenName "others" }
  everyone { TokenName "everyone" }
  team { TokenName "team" }
  role { TokenName "role" }
  choose { TokenName "choose" }
  from { TokenName "from" }
  then { TokenName "then" }
  know { TokenName "know" }
  look { TokenName "look" }
  at { TokenName "at"   }
  swap { TokenName "swap" }
  with { TokenName "with" }
  may { TokenName "may"  }
  if { TokenName "if" }
  chosen { TokenName "chosen" }
  center { TokenName "center" }
  extra { TokenName "extra" }
  union { TokenName "union" }
  intersect { TokenName "intersect" }
  card { TokenName "card" }
  defaulting { TokenName "defaulting" }
  to { TokenName "to" }
  named { TokenName "named" }
  cards { TokenName "cards" }
  all { TokenName "all" }
  race { TokenName "race" }
  last { TokenName "last" }
  players { TokenName "players" }
  who { TokenName "who" }
  directory { TokenName "directory" }
  races { TokenName "races" }
  teams { TokenName "teams" }
  roles { TokenName "roles" }
  connected { TokenName "connected" }
  awake { TokenName "awake" }
  '1' { TokenInt 1 }
  '2' { TokenInt 2 }
  '3' { TokenInt 3 }
  int { TokenInt $$ }
  decimal { TokenDecimal $$ }
  name { TokenName $$ }
%right union
%right intersect
%right ':'
%right of
%right then
%right who
%right may
%right or
%right and
%right not
%%
Action :: { ActionSpec }
Action : '(' Action ')' { $2 }
        -- | choose card from RelCardSet defaulting to RelCardPos
        --   then Action { AndThen (ChooseNFrom 1 $4 $7) $9 }
        | choose Int from RelCardSet defaulting to RelCardSet
          then Action { AndThen (ChooseNFrom $2 $4 $7) $9 }
        | know PlayerSet in play { KnowInPlay $2 }
        | look at RelCardSet { LookAt $3 }
        | swap RelCardPos with RelCardPos { SwapWith $2 $4 }
        | if Condition then Action { IfThen $2 $4 }
        | may Action { ActionOr NoOp $2 }
        | Action or Action { ActionOr $1 $3 }
        | Action then Action { AndThen $1 $3 }

PlayerSet :: { PlayerSetSpec }
PlayerSet : '(' PlayerSet ')' { $2 }
          | PlayerPos { FromPlayerSpecs $ Set.singleton $1 }
          | everyone { everyone }
          | players awake { PlayersSatisfying Awake }
          | players who Condition { PlayersSatisfying $3 }
          | players with role Role { PlayersSatisfying $4 }
          | not PlayerSet { PSDifference everyone $2 }
          | PlayerSet intersect PlayerSet { PSIntersection $1 $3 }
          | PlayerSet union PlayerSet { PSUnion $1 $3 }
          | adjacent {% parsePlayerSet $ lexer "left union right" }
          | others {% parsePlayerSet $ lexer "not self" }

PlayerPos :: { PlayerSpec }
PlayerPos : self { Self }
          | left { PSLeft }
          | right { PSRight }

Role :: { ConditionSpec }
Role : named name { RoleNameIs $2 }
     | race name { RoleRaceIs $2 }
     | team name { RoleTeamIs $2 }
     | not Role  { Not $2 }

RelCardPos :: { CardPosSpec }
RelCardPos : PlayerPos { PlayerRelative $1 }
           | center '1' { GlobalCP Center1 }
           | center '2' { GlobalCP Center2 }
           | center '3' { GlobalCP Center3 }
           | extra name { GlobalCP $ Extra $2  }
           | chosen card { LastChosen }

RelCardSet :: { CardSetSpec }
RelCardSet : '(' RelCardSet ')' { $2 }
           | RelCardPos { FromCardPosSpecs $ Set.singleton $1 }
           | cards of PlayerSet { OfPlayers $3 }
           | last chosen Int { LastNChosen $3 }
           | center cards
           { FromCardPosSpecs $             
             Set.fromList $
             GlobalCP `fmap`
             [ Center1, Center2, Center3 ] }
           | all cards { AllCards }
           | not RelCardSet { CSDifference AllCards $2 }
           | RelCardSet intersect RelCardSet { CSIntersection $1 $3 }
           | RelCardSet union RelCardSet { CSUnion $1 $3 }

Int :: { Integer }
Int : '1' { 1 }
    | '2' { 2 }
    | '3' { 3 }
    | int { $1 }

Number :: { NumSpec }
Number : Int { GlobalN $1 }
       | '#' of PlayerSet { SizePlayerSet $3 }
       | '#' of PlayerSet who voted for PlayerPos
       { SizePlayerSet $ PSIntersection $3 $ PlayersSatisfying $ VotedFor $7 }
       | '#' died of PlayerSet
       { SizePlayerSet $ PSIntersection $4 $ PlayersSatisfying Dead }
       | '#' voted for PlayerPos
       { SizePlayerSet $ PlayersSatisfying $ VotedFor $4 }

Op :: { Op }
Op : '=' { OpEq }
   | '/=' { OpNeq }
   | '<' { OpLt }
   | '>' { OpGt }
   | '<=' { OpLeq }
   | '>=' { OpGeq }

Condition :: { ConditionSpec }
Condition : '(' Condition ')' { $2 }
          | default { DefaultDeath }
          | for all of PlayerSet ':' Condition { AllOf $4 $6 }
          | for any of PlayerSet ':' Condition { AnyOf $4 $6 }
          | Number Op Number { Numeric $1 $2 $3 }
          -- | saw card of name
          -- { (($4 `elem`) .
          --    mapMaybe (\case
          --               (SeeThatCardIs _ face) -> Just face
          --               _                      -> Nothing)) `fmap`
          --   seenEvents
          -- }
          | not Condition { Not $2 }
          | Condition and Condition { And $1 $3 }
          | Condition or Condition { Or $1 $3 }
          -- | voted for PlayerPos { VotedFor $3 }
          | most voted { MostVoted }
          | any of PlayerSet died { AnyOf $3 Dead }
          | any of PlayerSet in play
          { Numeric (SizePlayerSet $3) OpGt (GlobalN 0) }

SetupFile :: { SetupRef -> Setup }
SetupFile : prompt ':' name
            finish ':' name
            order ':' IntOrDec
            time ':' int
            action ':' Action
            { \phaseName -> Setup { _phaseName = phaseName
                                  , _beginPrompt = $3
                                  , _finishPrompt = $6
                                  , _order = $9
                                  , _time = $12
                                  , _action = $15 } }

IntOrDec :: { Rational }
IntOrDec : Int     { fromInteger $1 }
         | decimal { $1 }

RoleFile :: { RoleRef -> RoleSpec }
RoleFile : team ':' name
           race ':' name
           ExtraCenter
           RoleSetups
           RoleDeaths
           ConnectedRoles
           {% do { let r = RoleSpec { _roleSpecName = ""
                                    , _roleRace = $6
                                    , _roleTeam = $3
                                    , _table = $7
                                    , _setupSpec = $8
                                    , _death = $9
                                    , _connectedRoles = $9 }
                 ; validateRole r
                 ; return $ \roleName -> set roleSpecName roleName r } }
RoleSetups :: { Set SetupRef }
RoleSetups : {- empty -}       { Set.empty }
           | setup ':' NameSet { $3 }

RoleDeaths :: { Set DeathRef }
RoleDeaths : {- empty -}       { Set.empty }
           | death ':' NameSet { $3 }

ConnectedRoles :: { Set RoleRef }
ConnectedRoles : {- empty -}                 { Set.empty }
               | connected roles ':' NameSet { $4 }

NameSet :: { Set String }
NameSet : NameList { Set.fromList $1 }

NameList :: { [String] }
NameList : {- empty -} { [] }
         | name NameList { $1 : $2 }

ExtraCenter :: { Map String RoleRef }
ExtraCenter : {- empty -} { Map.empty }
            | ExtraCenterItem ExtraCenter { Map.insert (fst $1) (snd $1) $2 }

ExtraCenterItem :: { (String, RoleRef) }
ExtraCenterItem : table ':' name '-' name { ($3, $5) }

GameFile :: { (FilePath, Set Race, Set TeamRef, [RoleRef]) }
GameFile : directory ':' Path
           races ':' NameSet
           teams ':' NameSet
           roles ':' NameList
           { ($3, $6, $9, $12) }

Path :: { FilePath }
Path : Drive RelativePath { joinDrive $1 $2 }

Drive :: { FilePath }
Drive : '/' { "/" }

RelativePath :: { FilePath }
RelativePath : name { $1 }
             | name '/' RelativePath { $1 </> $3 }
{
parseError :: [Token] -> ParserMonad a
parseError tokens = throwError $ "Parse error with tokens: " ++ show tokens
  
isValidFirst,isValidRest :: Char -> Bool
isValidFirst = isAlpha
isValidRest char = isAlphaNum char || char `elem` "-+_"

isValidName :: String -> Bool
isValidName "" = False
isValidName (c:cs) = isValidFirst c && all isValidRest cs

lexer :: String -> [Token]
lexer [] = []
lexer ('"':cs) = TokenName name : lexer rest
  where (name, _ : rest) = span (/= '"') cs
lexer ('#':cs) = TokenHash : lexer cs
lexer ('-':cs) = TokenHyphen : lexer cs
lexer (':':cs) = TokenColon : lexer cs
lexer ('=':cs) = TokenEq : lexer cs
lexer ('/':'=':cs) = TokenNeq : lexer cs
lexer ('/':cs) = TokenSlash : lexer cs
lexer ('<':'=':cs) = TokenLeq : lexer cs
lexer ('>':'=':cs) = TokenGeq : lexer cs
lexer ('<':cs) = TokenLt : lexer cs
lexer ('>':cs) = TokenGt : lexer cs
lexer ('(':cs) = TokenOpenBracket : lexer cs
lexer (')':cs) = TokenCloseBracket : lexer cs
lexer (c:cs)
      | isSpace c = lexer cs
      | isDigit c = lexNum (c:cs)
      | otherwise = TokenName (c:name) : lexer rest
      where (name, rest) = span isValidRest cs

lexNum :: String -> [Token]
lexNum cs =
  if isDecimal
  then TokenDecimal decimal : lexer rest
  else TokenInt (read num) : lexer afterNum
      where (num, afterNum) = span isDigit cs
            isDecimal = afterNum /= "" && head afterNum == '.'
            (fractional, rest) = span isDigit $ tail afterNum
            decimal = fst $ head $
                      readFloat $
                      num ++ '.' : fractional

parserToReader :: ParserMonadKinds m => Parser a -> String -> m a
parserToReader parser = view pm . parser . lexer

parserToRead :: Parser a -> Context -> String -> a
parserToRead parser context input =
  case runParser (parserToReader parser input) context of
    Left msg    -> error msg
    Right value -> value
    

parserToReaderFile :: (ParserMonadKinds m, MonadIO m) =>
                      Parser a -> FilePath -> m a
parserToReaderFile parser path =
  (liftIO (readFile path) >>= parserToReader parser) `catchError`
  \msg -> throwError $ path ++ ':' : msg

parserToReadFile :: (MonadIO m) => Parser a -> Context -> FilePath -> m a
parserToReadFile parser context path =
         parserToRead parser context <$> liftIO (readFile path)
}
