module Types where

TeamRef,RoleRef,DeathRef,SetupRef - unque names used to refer to teams, roles, death conditions and setup objects respectively
Context - lists of files present and ready to be interpreted, used to check if references to other entities (roles, teams, conditions) by name are valid
Setup - a single setup phase
RoleSpec - data specifying a single role
Role - a single role, something that each card is assigned
Card - internal representation of a card,
       contains a printed role and an actual role

type Perspective = (GameContext, Player)
-- type GameMaybePerspective = (GameContext, GameState, Maybe Player)
type GamePerspective = (GameContext, GameState, Player)
type GameData = (GameContext, GameState)

Number - entity that might be used in conditions as a natural number,
         can be evaluated to Integer from a GamePerspective
Op - allowed boolean relations on numbers
PlayerPosition - position specifying a player in relation to another Player
AbsoluteCardPosition - position specifying a particular card given GameData
RelativeCardPosition - position specifying a particular card given GamePerspective
Condition - an entity that can be evaluated for a GamePerspective into a boolean
GlobalCondition - an entity that can be evaluated for a GameData into a boolean
Player - id of a player, internally an integer,
         mappable to PlayerState given GameState
PlayerState - internal representation for each player,
              mappable from Player given GameState
PlayerSet - a set of players, from a given GamePerspective
AbsoluteCardSet - a set of cards, assuming GameData is known
CardSet - a set of cards, from a given GamePerspective
Action - modifies GameState from a given Perspective,
         if it involves Player choice, then it also specifies a default
