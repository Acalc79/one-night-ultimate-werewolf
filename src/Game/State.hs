{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
module Game.State where

import Control.Applicative (ZipList(..))
import Control.Monad (when)
import qualified Data.Map as Map

import Control.Monad.Reader (MonadReader, ask, local)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Random (MonadRandom)

import Control.Lens

import System.Random.Shuffle (shuffleM)

import Data.Core
import Data.Lenses
import Game.Spec (makeCard)

centerSize = 3

playerStateDefaults :: PlayerState
playerStateDefaults =
  PS { _initialRole = ""
     , _playerName = ""
     , _dead = False
     , _awake = True
     , _votesFor = Nothing
     , _knowledge = []
     }

positionsPermuation :: (MonadRandom m) => [Player] -> m [CardPos]
positionsPermuation players =
  shuffleM $ [Center1, Center2, Center3] ++ fmap OfPlayer players

makeInitialState :: (MonadReader GameContext m, MonadIO m, MonadRandom m) =>
                    [String] -> m GameState
makeInitialState playerNames = do
  ctx <- ask
  let numPlayers = (ctx^.gameCards.to length) - centerSize
  when (numPlayers <= 1) $
    error $ "Expected at least 5 cards to support 2 players, found " ++
            (ctx^.gameCards.to length.to show)
  when (length playerNames /= numPlayers) $
    error $ "There are cards for " ++ show numPlayers ++ " players, " ++
            "but " ++ show (length playerNames) ++
            " player names were supplied"
  positions <- positionsPermuation $ take numPlayers $ Player <$> [0..]
  let initialCards = Map.map (makeCard $ ctx^.gameRoles)
                             (Map.mapKeys Extra $ ctx^.gameTable) `Map.union`
                     Map.fromList (zip positions $ ctx^.gameCards)
  let players = Map.fromList $
                zipWith
                  (\p n -> makePlayer p n $ initialCards Map.! OfPlayer p)
                  (Player <$> [0..])
                  playerNames
  return GameState { _initialCards = initialCards
                   , _cards = initialCards
                   , _players = players
                   , _events = [] }
  where makePlayer :: Player -> String -> Card -> (Player, PlayerState)
        makePlayer p name card =
          (p, playerStateDefaults & playerName .~ name
                                  & initialRole .~ card^.nominalRole)
