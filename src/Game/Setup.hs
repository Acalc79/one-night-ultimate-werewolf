{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DuplicateRecordFields #-}
module Game.Setup where

import Control.Monad (when)
import Data.Foldable (traverse_)
import qualified Data.Foldable as Fold
import qualified Data.List as List
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)

import Control.Monad.Reader (runReader, runReaderT, MonadReader, ask)
import Control.Monad.State (MonadState, get, gets)
import Control.Monad.IO.Class (MonadIO, liftIO)

import Control.Lens

import Data.Time.Clock (UTCTime)

import Game.Eval
import Data.Core
import Data.Lenses
import Data.Printable
import Interface

runSetup :: (MonadIO m, MonadState GameState m, MonadReader GameContext m) => m ()
runSetup =
  views gameSetup (List.sortOn (view order) . Map.elems) >>=
  traverse_ runSinglePhase

runSinglePhase ::
  (MonadIO m, MonadState GameState m, MonadReader GameContext m) =>
  Setup -> m ()
runSinglePhase setupData = do
  performEvent $ NightPhaseStarts $ setupData^.phaseName
  players <- use players
  ctx <- ask
  let playerSet = Map.keysSet players
      wakesUp p = let rName = players^.player p.initialRole
                  in setupData^.phaseName `elem`
                     ctx^.gameRoles.mapLens rName.setup
      wokenPlayers = filter wakesUp $ Fold.toList playerSet
  liftIO $ showTo playerSet $ setupData^.beginPrompt
  deadline <- getDeadline $ setupData^.time.to fromIntegral
  Fold.traverse_ (\p -> performEvent $ PlayerEvent p WakeUp) wokenPlayers
  Fold.traverse_
    (\p -> Fold.for_ (playerSet Set.\\ Set.singleton p)
             (\p' -> performEvent $
                     PlayerEvent p $
                     if p' `elem` wokenPlayers then SeeAnotherAwake p'
                                               else SeeAnotherAsleep p'))
    wokenPlayers
  doWithTimer (setupData^.time) (showTo playerSet) (
    Fold.for_ wokenPlayers (\p ->
      runReaderT (doAction $ setupData^.action) (ctx, p, deadline)))
  liftIO $ showTo playerSet $ setupData^.finishPrompt
  Fold.for_ wokenPlayers (\p -> performEvent $ PlayerEvent p GoToSleep)
  performEvent $ NightPhaseEnds $ setupData^.phaseName

adjust :: ( MonadState GameState m
          , MonadReader (GameContext, Player, UTCTime) m) =>
          InGameView a -> m a
adjust (InView ma) = do
  (context, player, _)<- ask
  state <- get
  return $ runReader ma (context, state, player)

performEvent :: (MonadState GameState m) => Event -> m ()
performEvent event = do
  events %= (event :)
  case event of
    (PlayerEvent p e) -> do
      players.player p.knowledge %= (e :)
      case e of
        WakeUp -> players.player p.awake .= True
        GoToSleep -> players.player p.awake .= False
        SwapCards pos0 pos1 ->
          cards %= (\cards ->
          let card0 = cards^.pos pos0
              card1 = cards^.pos pos1
          in cards & pos pos1 .~ card0
                   & pos pos0 .~ card1)
        FlipCard p -> cards.pos p.isFaceUp %= not
        ChangeRoleOf p newRole -> cards.pos p.actualRole .= newRole
        _ -> return ()
    _ -> return ()

doAction ::
  ( MonadIO m, MonadState GameState m
  , MonadReader (GameContext, Player, UTCTime) m) =>
  ActionSpec -> m ()
doAction (NamedAction _ a) = doAction a
doAction NoOp = return ()
doAction (ChooseNFrom 0 _ _) = return ()
doAction (ChooseNFrom n set def) = do
  (_, p, deadline) <- ask
  choices <- adjust $ Fold.toList <$> eval set
  def <- adjust $ Fold.toList <$> eval def
  choices <- liftIO $
             toDeadlineWithDefault deadline def $
             chooseSeveralFrom (fromInteger n)
               ("Choose " ++ show n ++ " (defaults: " ++ show def ++ ")")
               show p choices
  Fold.for_ choices (performEvent . PlayerEvent p . ChooseCard)
doAction (KnowInPlay set) = do
  (_, p, _) <- ask
  list <- Fold.toList <$> adjust (eval set)
  Fold.traverse_ (performEvent . PlayerEvent p . SeeSignFrom) list
doAction (LookAt cardSet) = do
  p <- view _2
  game <- get
  list <- adjust $ Fold.toList <$> eval cardSet
  Fold.traverse_ performEvent $
    fmap (\cardPos -> PlayerEvent p $
                      SeeThatCardIs cardPos $
                      game^.cards . pos cardPos . nominalRole)
    list
doAction (SwapWith pos0 pos1) = do
  p <- view _2
  p0 <- adjust $ eval pos0
  p1 <- adjust $ eval pos1
  performEvent $ PlayerEvent p $ SwapCards p0 p1
doAction (IfThen c a) = do
  bool <- adjust $ eval c
  when bool (doAction a)
doAction (ActionOr a0 a1) = do
  (_, p, deadline) <- ask
  choice <- liftIO $
            toDeadlineWithDefault deadline a0 $
            getChoice "Choose one (default: 0)" pprint p [a0, a1]
  doAction choice
doAction (AndThen a0 a1) = doAction a0 >> doAction a1
