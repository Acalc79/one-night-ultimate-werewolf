{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Game.Finish where

import Control.Monad (when)
import qualified Data.Foldable as Fold
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Maybe (catMaybes)

import Control.Monad.Reader (runReader, ask, runReaderT, MonadReader)
import Control.Monad.State (MonadState, get)
import Control.Monad.IO.Class (MonadIO, liftIO)

import Control.Lens hiding (Context)
import Data.Set.Lens

import Data.Lenses
import Data.Core
import Game.Eval
import Interface (getChoice, showTo)

import Debug.Trace

playerSet :: (MonadState GameState m) => m (Set Player)
playerSet = use $ players.to Map.keysSet

getVotes :: forall m. (MonadIO m, MonadState GameState m) => Int -> m ()
getVotes time = do
  playerSet <- playerSet
  liftIO $ putStrLn $ "There are " ++ show (length playerSet) ++ " players"
  Fold.traverse_ getVoteFor playerSet
  where getVoteFor :: Player -> m ()
        getVoteFor p = do
          others <- filter (/= p) . Fold.toList  <$> playerSet
          thisVote <- liftIO $ getChoice "Your vote" show p others
          players.player p.votesFor ?= thisVote

deathFixpoint ::
  forall m. (MonadReader GameContext m, MonadState GameState m) => m ()
deathFixpoint = do
  playerData <- use players
  votes <- Map.traverseWithKey errorIfNothing $
           view votesFor <$>
           playerData
  newlyDeadPlayers <- imapM updateDeathState playerData
  when (newlyDeadPlayers /= playerData) $
    players .= newlyDeadPlayers >>
    deathFixpoint
  when (newlyDeadPlayers == playerData) $ do
    traceM "After death: "
    Fold.traverse_ (traceM . show) $ Map.toList newlyDeadPlayers
  where errorIfNothing :: Player -> Maybe Player -> m Player
        errorIfNothing p Nothing = error $ "No vote for player " ++ show p
        errorIfNothing p (Just vote) = return vote
        updateDeathState :: Player -> PlayerState -> m PlayerState
        updateDeathState p ps = do
          ctx <- ask
          death <- view $ gameDeath.to eval.gameView
          game <- get
          dead' <- runReaderT death (ctx, game, p)
          return $ ps & dead .~ dead'

determineWinners ::
  (MonadState GameState m, MonadReader GameContext m) =>
  m (Set Player)
determineWinners = do
  game <- get
  ctx <- ask
  let playerCondition p = view (to eval.gameView) $
                          (ctx^.gameTeams) Map.!
                          (game^.cards.pos (OfPlayer p).actualRole.team)
  return $ setOf
    (players.ifolded.asIndex.filtered (\p -> playerCondition p (ctx, game, p)))
      -- (\p -> let result :: Bool
      --            result = playerCondition p (ctx, game, p)
      --        in trace ("Player " ++ show p ++ " wins: " ++ show result) result))
    game

congratulate :: (MonadIO m, MonadState GameState m) => Set Player -> m ()
congratulate winners = do
  everyone <- use $ players.to Map.keysSet
  liftIO $ showTo everyone $ "The winners are: " ++ show winners
  history <- use events
  liftIO $ Fold.traverse_ print $ reverse history
  -- roles <- use cards
  -- liftIO $ Fold.traverse_ print (Map.toList roles)
