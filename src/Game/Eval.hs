{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Game.Eval where

import Control.Monad (filterM)
import qualified Data.Foldable as Fold
import Data.Traversable (for)
import Data.Maybe (fromMaybe)

import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Control.Monad.Reader.Class (local)

import Control.Lens
import Data.Set.Lens

import Data.Core
import Data.Lenses

class Evaluable a b | a -> b where
  eval :: a -> InGameView b

numPlayers :: InGameView Int
numPlayers = view $ _1.gameCards.to length

shiftBy :: Int -> Player -> InGameView Player
shiftBy k (Player m) = do
  n <- numPlayers
  return $ Player $ (m + k) `mod` n

everyone :: InGameView (Set Player)
everyone = view $ _2.players.to Map.keysSet

instance Evaluable PlayerSpec Player where
  eval Self = view _3
  eval PSLeft = eval Self >>= shiftBy (-1)
  eval PSRight = eval Self >>= shiftBy 1

asPlayer :: Player -> InGameView a -> InGameView a
asPlayer p = local $ set _3 p

instance Evaluable PlayerSetSpec (Set Player) where
  eval (FromPlayerSpecs set) = Set.fromList <$> traverse eval (set^..folded)
  eval (PlayersSatisfying condition) = do
    players <- Fold.toList <$> everyone
    Set.fromList <$> filterM (\p -> asPlayer p $ eval condition) players
  eval (PSDifference set0 set1) = Set.difference <$> eval set0 <*> eval set1
  eval (PSUnion set0 set1) = Set.union <$> eval set0 <*> eval set1
  eval (PSIntersection set0 set1) =
    Set.intersection <$> eval set0 <*> eval set1

instance Evaluable CardPosSpec CardPos where
  eval (GlobalCP cp) = return cp
  eval (PlayerRelative pSpec) = OfPlayer <$> eval pSpec
  eval LastChosen = do
    p <- view _3
    history <- view $ _2.players.player p.knowledge
    return $
      fromMaybe (error "Asked for last chosen card with no cards chosen") $
      history^?folded._ChooseCard

instance Evaluable CardSetSpec (Set CardPos) where
  eval (FromCardPosSpecs set) = Set.fromList <$> traverse eval (set^..folded)
  eval (OfPlayers set) = Set.map OfPlayer <$> eval set
  eval (LastNChosen n) = do
    let k = fromInteger n
    p <- view _3
    history <- view $ _2.players.player p.knowledge
    let chosen = history^..folded._ChooseCard
    if length chosen < k
      then error $ "Asked for " ++ show k ++
                   " chosen cards with only " ++ show (length chosen) ++
                   " available"
      else return $ Set.fromList $ take k chosen
  eval AllCards = view $ _2.cards.to Map.keysSet
  eval (CSDifference set0 set1) = Set.difference <$> eval set0 <*> eval set1
  eval (CSUnion set0 set1) = Set.union <$> eval set0 <*> eval set1
  eval (CSIntersection set0 set1) =
    Set.intersection <$> eval set0 <*> eval set1

instance Evaluable Op (Integer -> Integer -> Bool) where
  eval OpEq = return (==)
  eval OpNeq = return (/=)
  eval OpLt = return (<)
  eval OpGt = return (>)
  eval OpLeq = return (<=)
  eval OpGeq = return (>=)

instance Evaluable NumSpec Integer where
  eval (GlobalN n) = return n
  eval (SizePlayerSet set) = toInteger . length <$> eval set

numVotesFor :: Player -> InGameView Int
numVotesFor p = lengthOf (folded.votesFor._Just.only p) <$> view (_2.players)

currentPlayer :: InGameView PlayerState
currentPlayer = do
  p <- view _3
  view $ _2.players.player p

instance Evaluable ConditionSpec Bool where
  eval Awake = view awake <$> currentPlayer
  eval Dead = view dead <$> currentPlayer
  eval DefaultDeath = view (_1.defaultDeath) >>= eval
  eval (AllOf pSpecs cSpec) = do
    players <- Fold.toList <$> eval pSpecs
    and <$> for players (\p -> asPlayer p $ eval cSpec)
  eval (AnyOf pSpecs cSpec) = do
    players <- Fold.toList <$> eval pSpecs
    or <$> for players (\p -> asPlayer p $ eval cSpec)
  eval (Numeric m op n) = eval op <*> eval m <*> eval n
  eval (VotedFor pSpec) = do
    p' <- eval pSpec
    ps <- view $ _2.players
    p <- view _3
    return $ notNullOf (player p.votesFor._Just.only p') ps
  eval MostVoted = do
    p <- view _3
    ps <- view $ _2.players
    let votesForP = lengthOf (folded.votesFor._Just.only p) ps
    and <$> for (Map.keys ps) (fmap (<= votesForP) . numVotesFor)
  eval (RoleNameIs name) = notNullOf (initialRole.only name) <$> currentPlayer
  eval (RoleRaceIs name) = do
    role <- view initialRole <$> currentPlayer
    raceName <- view $ _1.gameRoles.mapLens role.race
    return $ raceName == name
  eval (RoleTeamIs name) = do
    role <- view initialRole <$> currentPlayer
    teamName <- view $ _1.gameRoles.mapLens role.team
    return $ teamName == name
  eval (Not c) = not <$> eval c
  eval (And c0 c1) = (&&) <$> eval c0 <*> eval c1
  eval (Or c0 c1) = (||) <$> eval c0 <*> eval c1
