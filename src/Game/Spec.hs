{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DuplicateRecordFields #-}
module Game.Spec where

import Control.Monad (join, filterM)
import Control.Exception.Base (IOException(..))
import qualified Data.Foldable as Fold
import qualified Data.List.Extra as List
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map

import Control.Monad.Except (runExceptT, MonadError, throwError)
import Control.Monad.Reader (MonadReader, runReader, runReaderT)
import Control.Monad.IO.Class (MonadIO, liftIO)

import Control.Lens hiding (Context)
import Data.Set.Lens

import System.FilePath (FilePath, combine, (</>))
import System.Directory (listDirectory, doesFileExist)

import Data.Core
import Data.Lenses
import Data.Parser
  ( ErrorType, isValidName
  , parseSetup, parseRoleSpec,  parseCondition, parseGameFile
  , parserToReaderFile, parserToReadFile)

readRoles :: forall m. ( MonadError ErrorType m
                       , MonadReader Context m
                       , MonadIO m) =>
             FilePath -> Set RoleRef -> m (Set RoleSpec)
readRoles path roleSpecs = do
  roles <- traverse (\name -> fmap ($ name) $
                              parserToReaderFile parseRoleSpec $
                              path </> roleSubdir </> name) $
           Fold.toList roleSpecs
  let stillNeeded = (Set.\\ roleSpecs) $
                    setOf (folded.connectedRoles.folded) roles
  let roleSet = Set.fromList roles
  if null stillNeeded then return roleSet
                      else Set.union roleSet <$> readRoles path stillNeeded

specToRole :: RoleSpec -> Role
specToRole rSpec =
  Role { _roleName = rSpec^.roleSpecName
       , _race = rSpec^.roleRace
       , _team = rSpec^.roleTeam
       , _setup = rSpec^.setupSpec }

makeTable :: Set RoleSpec -> Map String RoleRef
makeTable roleSpecs = if List.anySame names
                      then error "Extra center cards don't have unique names"
                      else Map.unions maps
  where maps = roleSpecs^..folded.table
        names = maps >>= Map.keys

listFilesFromSubdir :: forall m. (MonadIO m) =>
                       FilePath -> FilePath -> m [String]
listFilesFromSubdir parent name = do
  let path = parent </> name
  allContent <- liftIO $ listDirectory path
  filterM (condition path) allContent
  where condition :: FilePath -> FilePath -> m Bool
        condition path name =
          liftIO $
          fmap (&& isValidName name) $
          doesFileExist $
          combine path name

teamSubdir = "team"
roleSubdir = "role"
setupSubdir = "setup"
deathSubdir = "death"
defaultName = "default"

readGame :: (MonadIO m) => FilePath -> m GameContext
readGame path = do
  (dir, gameRaces, declaredTeams, declaredRoles) <-
       parserToReadFile parseGameFile emptyContext path
  let teams' = Fold.toList declaredTeams
  roles' <- listFilesFromSubdir dir roleSubdir
  setups' <- listFilesFromSubdir dir roleSubdir
  deaths' <- listFilesFromSubdir dir deathSubdir
  let ctx = Context { _teams = teams'
                    , _roles = roles'
                    , _setups = setups'
                    , _deaths = deaths' }
  result <- runExceptT $ flip runReaderT ctx (do
    roleSpecsSet <- readRoles dir $ Set.fromList declaredRoles
    let roleSpecs = Fold.toList roleSpecsSet
    gameTeams <- Map.fromList <$>
                 traverse (\ref -> fmap (ref,) $
                                   parserToReaderFile parseCondition $
                                   dir </> teamSubdir </> ref) teams'
    gameSetup <- fmap Map.fromList $
                 traverse (\ref -> fmap (\setup -> (ref, setup ref)) $
                                   parserToReaderFile parseSetup $
                                   dir </> setupSubdir </> ref) $
                 Fold.toList $ Set.unions $
                 view setupSpec <$> roleSpecs
    defaultDeath <- parserToReaderFile parseCondition $
                    dir </> deathSubdir </> defaultName
    let gameDeathRef = case List.nub $ roleSpecs^..folded.death.folded of
         []     -> defaultName
         [rule] -> rule
         _      -> error "Handling multiple death rules not implemented"
    gameDeath <- parserToReaderFile parseCondition $
                 dir </> deathSubdir </> gameDeathRef
    let gameRoles = Map.fromList $
                    (\r -> (r^.roleSpecName, specToRole r)) <$>
                    roleSpecs
    return GameContext { _gameTeams = gameTeams
                       , _gameRaces = gameRaces
                       , _gameRoles = gameRoles
                       , _gameSetup = gameSetup
                       , _gameTable = makeTable roleSpecsSet
                       , _gameCards = makeCard gameRoles <$> declaredRoles
                       , _defaultDeath = defaultDeath
                       , _gameDeath= gameDeath })
  case result of
    Left errorMsg -> error errorMsg
    Right value -> return value

makeCard :: Map RoleRef Role -> RoleRef -> Card
makeCard roleMap role = Card { _nominalRole = role
                             , _actualRole = roleMap Map.! role
                             , _isFaceUp = False }
