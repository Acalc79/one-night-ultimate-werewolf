{-# OPTIONS_GHC -w #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}
module Data.Parser
  ( ErrorType, isValidName
  , parseAction, parseCondition
  , parseSetup, parseRoleSpec, parseGameFile
  , parserToReaderFile, parserToReadFile, runParser, runParserT) where

import Control.Monad (join)
import Control.Monad.IO.Class (MonadIO, liftIO)
import qualified Data.Foldable as Fold
import Data.Maybe (mapMaybe)
import Numeric (readFloat)
import Data.Char (isSpace, isDigit, isAlpha, isAlphaNum)

import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map

import Control.Monad.Reader (MonadReader, ask, asks)
import Control.Monad.Except (throwError, catchError)

import System.FilePath ((</>), joinDrive)

import Control.Lens hiding (Context)

import Data.Core
import Data.Lenses
import Data.Parser.Core
import Data.Parser.Function
import qualified Data.Array as Happy_Data_Array
import qualified Data.Bits as Bits
import Control.Applicative(Applicative(..))
import Control.Monad (ap)

-- parser produced by Happy Version 1.19.12

data HappyAbsSyn 
	= HappyTerminal (Token)
	| HappyErrorToken Int
	| HappyAbsSyn10 (ActionSpec)
	| HappyAbsSyn11 (PlayerSetSpec)
	| HappyAbsSyn12 (PlayerSpec)
	| HappyAbsSyn13 (ConditionSpec)
	| HappyAbsSyn14 (CardPosSpec)
	| HappyAbsSyn15 (CardSetSpec)
	| HappyAbsSyn16 (Integer)
	| HappyAbsSyn17 (NumSpec)
	| HappyAbsSyn18 (Op)
	| HappyAbsSyn20 (SetupRef -> Setup)
	| HappyAbsSyn21 (Rational)
	| HappyAbsSyn22 (RoleRef -> RoleSpec)
	| HappyAbsSyn23 (Set SetupRef)
	| HappyAbsSyn24 (Set DeathRef)
	| HappyAbsSyn25 (Set RoleRef)
	| HappyAbsSyn26 (Set String)
	| HappyAbsSyn27 ([String])
	| HappyAbsSyn28 (Map String RoleRef)
	| HappyAbsSyn29 ((String, RoleRef))
	| HappyAbsSyn30 ((FilePath, Set Race, Set TeamRef, [RoleRef]))
	| HappyAbsSyn31 (FilePath)

{- to allow type-synonyms as our monads (likely
 - with explicitly-specified bind and return)
 - in Haskell98, it seems that with
 - /type M a = .../, then /(HappyReduction M)/
 - is not allowed.  But Happy is a
 - code-generator that can just substitute it.
type HappyReduction m = 
	   Int 
	-> (Token)
	-> HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> m HappyAbsSyn)
	-> [HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> m HappyAbsSyn)] 
	-> HappyStk HappyAbsSyn 
	-> [(Token)] -> m HappyAbsSyn
-}

action_0,
 action_1,
 action_2,
 action_3,
 action_4,
 action_5,
 action_6,
 action_7,
 action_8,
 action_9,
 action_10,
 action_11,
 action_12,
 action_13,
 action_14,
 action_15,
 action_16,
 action_17,
 action_18,
 action_19,
 action_20,
 action_21,
 action_22,
 action_23,
 action_24,
 action_25,
 action_26,
 action_27,
 action_28,
 action_29,
 action_30,
 action_31,
 action_32,
 action_33,
 action_34,
 action_35,
 action_36,
 action_37,
 action_38,
 action_39,
 action_40,
 action_41,
 action_42,
 action_43,
 action_44,
 action_45,
 action_46,
 action_47,
 action_48,
 action_49,
 action_50,
 action_51,
 action_52,
 action_53,
 action_54,
 action_55,
 action_56,
 action_57,
 action_58,
 action_59,
 action_60,
 action_61,
 action_62,
 action_63,
 action_64,
 action_65,
 action_66,
 action_67,
 action_68,
 action_69,
 action_70,
 action_71,
 action_72,
 action_73,
 action_74,
 action_75,
 action_76,
 action_77,
 action_78,
 action_79,
 action_80,
 action_81,
 action_82,
 action_83,
 action_84,
 action_85,
 action_86,
 action_87,
 action_88,
 action_89,
 action_90,
 action_91,
 action_92,
 action_93,
 action_94,
 action_95,
 action_96,
 action_97,
 action_98,
 action_99,
 action_100,
 action_101,
 action_102,
 action_103,
 action_104,
 action_105,
 action_106,
 action_107,
 action_108,
 action_109,
 action_110,
 action_111,
 action_112,
 action_113,
 action_114,
 action_115,
 action_116,
 action_117,
 action_118,
 action_119,
 action_120,
 action_121,
 action_122,
 action_123,
 action_124,
 action_125,
 action_126,
 action_127,
 action_128,
 action_129,
 action_130,
 action_131,
 action_132,
 action_133,
 action_134,
 action_135,
 action_136,
 action_137,
 action_138,
 action_139,
 action_140,
 action_141,
 action_142,
 action_143,
 action_144,
 action_145,
 action_146,
 action_147,
 action_148,
 action_149,
 action_150,
 action_151,
 action_152,
 action_153,
 action_154,
 action_155,
 action_156,
 action_157,
 action_158,
 action_159,
 action_160,
 action_161,
 action_162,
 action_163,
 action_164,
 action_165,
 action_166,
 action_167,
 action_168,
 action_169,
 action_170,
 action_171,
 action_172,
 action_173,
 action_174,
 action_175,
 action_176,
 action_177,
 action_178,
 action_179,
 action_180,
 action_181,
 action_182,
 action_183,
 action_184,
 action_185,
 action_186,
 action_187,
 action_188,
 action_189,
 action_190,
 action_191,
 action_192,
 action_193,
 action_194,
 action_195,
 action_196,
 action_197,
 action_198,
 action_199,
 action_200,
 action_201,
 action_202,
 action_203,
 action_204,
 action_205,
 action_206,
 action_207,
 action_208,
 action_209,
 action_210,
 action_211,
 action_212,
 action_213,
 action_214,
 action_215,
 action_216,
 action_217,
 action_218,
 action_219,
 action_220,
 action_221,
 action_222,
 action_223,
 action_224,
 action_225,
 action_226,
 action_227,
 action_228 :: () => Int -> ({-HappyReduction (ParserMonad) = -}
	   Int 
	-> (Token)
	-> HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> (ParserMonad) HappyAbsSyn)
	-> [HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> (ParserMonad) HappyAbsSyn)] 
	-> HappyStk HappyAbsSyn 
	-> [(Token)] -> (ParserMonad) HappyAbsSyn)

happyReduce_7,
 happyReduce_8,
 happyReduce_9,
 happyReduce_10,
 happyReduce_11,
 happyReduce_12,
 happyReduce_13,
 happyReduce_14,
 happyReduce_15,
 happyReduce_16,
 happyReduce_17,
 happyReduce_18,
 happyReduce_19,
 happyReduce_20,
 happyReduce_21,
 happyReduce_22,
 happyReduce_23,
 happyReduce_24,
 happyReduce_25,
 happyReduce_26,
 happyReduce_27,
 happyReduce_28,
 happyReduce_29,
 happyReduce_30,
 happyReduce_31,
 happyReduce_32,
 happyReduce_33,
 happyReduce_34,
 happyReduce_35,
 happyReduce_36,
 happyReduce_37,
 happyReduce_38,
 happyReduce_39,
 happyReduce_40,
 happyReduce_41,
 happyReduce_42,
 happyReduce_43,
 happyReduce_44,
 happyReduce_45,
 happyReduce_46,
 happyReduce_47,
 happyReduce_48,
 happyReduce_49,
 happyReduce_50,
 happyReduce_51,
 happyReduce_52,
 happyReduce_53,
 happyReduce_54,
 happyReduce_55,
 happyReduce_56,
 happyReduce_57,
 happyReduce_58,
 happyReduce_59,
 happyReduce_60,
 happyReduce_61,
 happyReduce_62,
 happyReduce_63,
 happyReduce_64,
 happyReduce_65,
 happyReduce_66,
 happyReduce_67,
 happyReduce_68,
 happyReduce_69,
 happyReduce_70,
 happyReduce_71,
 happyReduce_72,
 happyReduce_73,
 happyReduce_74,
 happyReduce_75,
 happyReduce_76,
 happyReduce_77,
 happyReduce_78,
 happyReduce_79,
 happyReduce_80,
 happyReduce_81,
 happyReduce_82,
 happyReduce_83,
 happyReduce_84,
 happyReduce_85,
 happyReduce_86,
 happyReduce_87,
 happyReduce_88,
 happyReduce_89,
 happyReduce_90,
 happyReduce_91,
 happyReduce_92,
 happyReduce_93,
 happyReduce_94,
 happyReduce_95 :: () => ({-HappyReduction (ParserMonad) = -}
	   Int 
	-> (Token)
	-> HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> (ParserMonad) HappyAbsSyn)
	-> [HappyState (Token) (HappyStk HappyAbsSyn -> [(Token)] -> (ParserMonad) HappyAbsSyn)] 
	-> HappyStk HappyAbsSyn 
	-> [(Token)] -> (ParserMonad) HappyAbsSyn)

happyExpList :: Happy_Data_Array.Array Int Int
happyExpList = Happy_Data_Array.listArray (0,420) ([0,0,512,0,45568,6,0,0,0,257,18992,0,0,1920,0,0,128,33792,31,16384,0,0,16384,0,0,0,0,480,0,0,512,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,4096,0,0,0,4,0,0,0,0,0,0,2,0,1714,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,16384,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,33856,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,2,32272,0,256,0,0,0,1,16136,0,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,130,0,0,250,0,0,0,0,0,0,0,6144,0,0,0,0,16384,64,4748,0,0,480,0,0,0,0,0,0,0,0,0,0,2048,0,0,0,0,0,0,32,0,0,0,0,1024,49156,296,0,0,30,0,0,16384,0,0,32,0,0,0,0,32,4,0,0,0,0,0,0,0,49152,3,0,16384,0,4034,0,32,0,0,0,0,0,4,0,0,0,0,0,112,448,0,0,0,2048,0,51200,26,0,0,0,1028,10432,1,0,7680,0,0,0,24576,2048,0,0,0,0,0,8192,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8192,0,0,0,0,0,0,0,16,0,0,0,0,0,0,0,56,0,0,0,0,0,0,128,0,0,4,7200,28672,352,0,0,0,0,1,49152,0,0,0,0,0,0,2,0,0,0,32768,0,32768,428,0,0,0,16384,0,16384,214,0,0,0,0,0,16,0,0,0,0,0,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,512,4096,126,0,1,0,0,512,12288,0,0,0,0,32768,128,9496,0,0,960,0,16384,64,4748,0,0,480,0,8192,0,0,0,0,240,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8192,0,0,0,0,8224,17920,9,0,61440,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2048,0,0,384,0,0,0,512,4096,126,0,1,0,0,256,2048,63,32768,0,0,0,0,16384,0,0,0,0,0,0,4096,0,0,0,0,0,32,57600,7,4096,0,0,0,0,0,0,0,256,0,0,0,0,0,0,128,0,4096,0,0,0,0,0,0,0,4,64,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,512,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,1,0,0,0,0,0,0,0,0,0,4,0,0,0,0,14,0,0,0,0,256,2048,63,32768,0,0,0,0,0,0,32,0,0,0,0,0,0,24,0,0,0,0,0,0,0,0,0,0,0,768,0,0,0,0,0,0,64,2,288,0,0,0,0,0,0,0,0,0,0,0,96,0,0,0,0,0,0,16,0,0,0,0,0,0,0,0,0,0,0,0,12288,0,6144,0,0,0,8192,0,2017,0,16,0,0,4096,32768,1008,0,8,0,0,0,0,8193,0,0,0,0,0,32768,0,0,0,0,0,512,4096,14,45112,0,0,0,0,256,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,0,0,0,32,57600,32768,2819,0,0,0,16,28800,49152,1409,0,0,0,0,0,0,64,28,0,0,0,512,0,0,0,0,0,0,0,0,16,0,0,0,0,0,1024,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,28,112,0,0,0,512,0,45568,6,0,0,0,0,8192,1024,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,480,0,0,0,0,0,0,0,0,0,16,61568,3,2048,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0,49152,0,0,0,0,1,1800,7168,88,0,0,32768,0,900,3584,44,0,0,0,0,0,0,0,0,0,0,0,0,11264,0,0,0,32768,0,0,1536,0,0,0,16384,0,0,768,0,0,0,0,0,0,0,0,0,0,0,512,0,0,0,0,0,0,0,0,0,0,0,0,0,1024,32,4608,0,0,0,0,0,0,0,1024,0,0,0,0,0,0,512,0,0,0,0,0,0,256,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,0,8,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,128,0,0,0,0,0,0,32768,0,0,0,0,0,0,0,0,0,0,32768,0,0,0,0,0,0,16384,0,0,0,0,0,0,8192,0,0,0,0,0,0,4096,0,0,0,8192,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,512,24578,148,0,0,15,0,256,12289,74,0,32768,7,0,0,0,0,0,1,0,0,0,0,0,4096,0,0,0,0,0,0,3072,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,512,4096,14,45112,0,0,0,0,12288,0,0,0,0,0,0,6144,0,0,0,0,0,0,49152,1,0,0,0,0,2048,0,0,0,0,0,0,8192,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8192,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,128,0,0,0,0,0,0,32,0,0,0,0,0,16384,0,0,0,0,0,0,8192,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1024,96,0,0,0,128,0,44160,1,0,0,0,0,0,0,0,992,0,0,0,0,0,0,512,0,0,0,0,0,0,0,0,0,16384,0,0,0,0,0,0,32,0,0,0,0,0,0,0,0,0,0,32,0,0,0,0,0,4096,0,0,0,0,0,0,0,8,0,0,0,0,0,2048,0,0,0,1,0,0,0,0,0,16384,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,8192,1024,0,0,0,0,1024,0,0,0,0,0,0,0,0,0,0,1024,0,0,0,0,0,0,512,0,0,0,0,0,0,0,0,0,0,0,0,32768,0,0,0,0,0,0,0,0,0,0,16,0,0,0,0,0,0,0,0,0,0,16,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16384,0,0,0,8,0,0,0,0,0,0,0,0,0,16384,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1024,0,0,0,0,0,0,64,0,54848,0,0,0,0,0,1024,128,0,0,0
	])

{-# NOINLINE happyExpListPerState #-}
happyExpListPerState st =
    token_strs_expected
  where token_strs = ["error","%dummy","%start_parseAction","%start_parseCondition","%start_parsePlayerSet","%start_parseNumber","%start_parseSetup","%start_parseRoleSpec","%start_parseGameFile","Action","PlayerSet","PlayerPos","Role","RelCardPos","RelCardSet","Int","Number","Op","Condition","SetupFile","IntOrDec","RoleFile","RoleSetups","RoleDeaths","ConnectedRoles","NameSet","NameList","ExtraCenter","ExtraCenterItem","GameFile","Path","Drive","RelativePath","'#'","'='","'/'","'/='","'<'","'>'","'<='","'>='","'('","')'","'-'","':'","prompt","finish","order","time","action","table","setup","death","default","any","died","in","play","most","voted","not","and","or","for","of","self","left","right","adjacent","others","everyone","team","role","choose","from","then","know","look","at","swap","with","may","if","chosen","center","extra","union","intersect","card","defaulting","to","named","cards","all","race","last","players","who","directory","races","teams","roles","connected","awake","'1'","'2'","'3'","int","decimal","name","%eof"]
        bit_start = st * 111
        bit_end = (st + 1) * 111
        read_bit = readArrayBit happyExpList
        bits = map read_bit [bit_start..bit_end - 1]
        bits_indexed = zip bits [0..110]
        token_strs_expected = concatMap f bits_indexed
        f (False, _) = []
        f (True, nr) = [token_strs !! nr]

action_0 (42) = happyShift action_8
action_0 (74) = happyShift action_42
action_0 (77) = happyShift action_43
action_0 (78) = happyShift action_44
action_0 (80) = happyShift action_45
action_0 (82) = happyShift action_46
action_0 (83) = happyShift action_47
action_0 (10) = happyGoto action_41
action_0 _ = happyFail (happyExpListPerState 0)

action_1 (34) = happyShift action_17
action_1 (42) = happyShift action_35
action_1 (54) = happyShift action_36
action_1 (55) = happyShift action_37
action_1 (59) = happyShift action_38
action_1 (61) = happyShift action_39
action_1 (64) = happyShift action_40
action_1 (105) = happyShift action_18
action_1 (106) = happyShift action_19
action_1 (107) = happyShift action_20
action_1 (108) = happyShift action_21
action_1 (16) = happyGoto action_15
action_1 (17) = happyGoto action_33
action_1 (19) = happyGoto action_34
action_1 _ = happyFail (happyExpListPerState 1)

action_2 (42) = happyShift action_24
action_2 (61) = happyShift action_25
action_2 (66) = happyShift action_26
action_2 (67) = happyShift action_27
action_2 (68) = happyShift action_28
action_2 (69) = happyShift action_29
action_2 (70) = happyShift action_30
action_2 (71) = happyShift action_31
action_2 (97) = happyShift action_32
action_2 (11) = happyGoto action_22
action_2 (12) = happyGoto action_23
action_2 _ = happyFail (happyExpListPerState 2)

action_3 (34) = happyShift action_17
action_3 (105) = happyShift action_18
action_3 (106) = happyShift action_19
action_3 (107) = happyShift action_20
action_3 (108) = happyShift action_21
action_3 (16) = happyGoto action_15
action_3 (17) = happyGoto action_16
action_3 _ = happyFail (happyExpListPerState 3)

action_4 (46) = happyShift action_14
action_4 (20) = happyGoto action_13
action_4 _ = happyFail (happyExpListPerState 4)

action_5 (72) = happyShift action_12
action_5 (22) = happyGoto action_11
action_5 _ = happyFail (happyExpListPerState 5)

action_6 (99) = happyShift action_10
action_6 (30) = happyGoto action_9
action_6 _ = happyFail (happyExpListPerState 6)

action_7 (42) = happyShift action_8
action_7 _ = happyFail (happyExpListPerState 7)

action_8 (42) = happyShift action_8
action_8 (74) = happyShift action_42
action_8 (77) = happyShift action_43
action_8 (78) = happyShift action_44
action_8 (80) = happyShift action_45
action_8 (82) = happyShift action_46
action_8 (83) = happyShift action_47
action_8 (10) = happyGoto action_88
action_8 _ = happyFail (happyExpListPerState 8)

action_9 (111) = happyAccept
action_9 _ = happyFail (happyExpListPerState 9)

action_10 (45) = happyShift action_87
action_10 _ = happyFail (happyExpListPerState 10)

action_11 (111) = happyAccept
action_11 _ = happyFail (happyExpListPerState 11)

action_12 (45) = happyShift action_86
action_12 _ = happyFail (happyExpListPerState 12)

action_13 (111) = happyAccept
action_13 _ = happyFail (happyExpListPerState 13)

action_14 (45) = happyShift action_85
action_14 _ = happyFail (happyExpListPerState 14)

action_15 _ = happyReduce_53

action_16 (111) = happyAccept
action_16 _ = happyFail (happyExpListPerState 16)

action_17 (56) = happyShift action_82
action_17 (60) = happyShift action_83
action_17 (65) = happyShift action_84
action_17 _ = happyFail (happyExpListPerState 17)

action_18 _ = happyReduce_49

action_19 _ = happyReduce_50

action_20 _ = happyReduce_51

action_21 _ = happyReduce_52

action_22 (87) = happyShift action_80
action_22 (88) = happyShift action_81
action_22 (111) = happyAccept
action_22 _ = happyFail (happyExpListPerState 22)

action_23 _ = happyReduce_17

action_24 (42) = happyShift action_24
action_24 (61) = happyShift action_25
action_24 (66) = happyShift action_26
action_24 (67) = happyShift action_27
action_24 (68) = happyShift action_28
action_24 (69) = happyShift action_29
action_24 (70) = happyShift action_30
action_24 (71) = happyShift action_31
action_24 (97) = happyShift action_32
action_24 (11) = happyGoto action_79
action_24 (12) = happyGoto action_23
action_24 _ = happyFail (happyExpListPerState 24)

action_25 (42) = happyShift action_24
action_25 (61) = happyShift action_25
action_25 (66) = happyShift action_26
action_25 (67) = happyShift action_27
action_25 (68) = happyShift action_28
action_25 (69) = happyShift action_29
action_25 (70) = happyShift action_30
action_25 (71) = happyShift action_31
action_25 (97) = happyShift action_32
action_25 (11) = happyGoto action_78
action_25 (12) = happyGoto action_23
action_25 _ = happyFail (happyExpListPerState 25)

action_26 _ = happyReduce_27

action_27 _ = happyReduce_28

action_28 _ = happyReduce_29

action_29 _ = happyReduce_25

action_30 _ = happyReduce_26

action_31 _ = happyReduce_18

action_32 (81) = happyShift action_75
action_32 (98) = happyShift action_76
action_32 (104) = happyShift action_77
action_32 _ = happyFail (happyExpListPerState 32)

action_33 (35) = happyShift action_69
action_33 (37) = happyShift action_70
action_33 (38) = happyShift action_71
action_33 (39) = happyShift action_72
action_33 (40) = happyShift action_73
action_33 (41) = happyShift action_74
action_33 (18) = happyGoto action_68
action_33 _ = happyFail (happyExpListPerState 33)

action_34 (62) = happyShift action_66
action_34 (63) = happyShift action_67
action_34 (111) = happyAccept
action_34 _ = happyFail (happyExpListPerState 34)

action_35 (34) = happyShift action_17
action_35 (42) = happyShift action_35
action_35 (54) = happyShift action_36
action_35 (55) = happyShift action_37
action_35 (59) = happyShift action_38
action_35 (61) = happyShift action_39
action_35 (64) = happyShift action_40
action_35 (105) = happyShift action_18
action_35 (106) = happyShift action_19
action_35 (107) = happyShift action_20
action_35 (108) = happyShift action_21
action_35 (16) = happyGoto action_15
action_35 (17) = happyGoto action_33
action_35 (19) = happyGoto action_65
action_35 _ = happyFail (happyExpListPerState 35)

action_36 _ = happyReduce_65

action_37 (65) = happyShift action_64
action_37 _ = happyFail (happyExpListPerState 37)

action_38 (60) = happyShift action_63
action_38 _ = happyFail (happyExpListPerState 38)

action_39 (34) = happyShift action_17
action_39 (42) = happyShift action_35
action_39 (54) = happyShift action_36
action_39 (55) = happyShift action_37
action_39 (59) = happyShift action_38
action_39 (61) = happyShift action_39
action_39 (64) = happyShift action_40
action_39 (105) = happyShift action_18
action_39 (106) = happyShift action_19
action_39 (107) = happyShift action_20
action_39 (108) = happyShift action_21
action_39 (16) = happyGoto action_15
action_39 (17) = happyGoto action_33
action_39 (19) = happyGoto action_62
action_39 _ = happyFail (happyExpListPerState 39)

action_40 (55) = happyShift action_60
action_40 (94) = happyShift action_61
action_40 _ = happyFail (happyExpListPerState 40)

action_41 (63) = happyShift action_58
action_41 (76) = happyShift action_59
action_41 (111) = happyAccept
action_41 _ = happyFail (happyExpListPerState 41)

action_42 (105) = happyShift action_18
action_42 (106) = happyShift action_19
action_42 (107) = happyShift action_20
action_42 (108) = happyShift action_21
action_42 (16) = happyGoto action_57
action_42 _ = happyFail (happyExpListPerState 42)

action_43 (42) = happyShift action_24
action_43 (61) = happyShift action_25
action_43 (66) = happyShift action_26
action_43 (67) = happyShift action_27
action_43 (68) = happyShift action_28
action_43 (69) = happyShift action_29
action_43 (70) = happyShift action_30
action_43 (71) = happyShift action_31
action_43 (97) = happyShift action_32
action_43 (11) = happyGoto action_56
action_43 (12) = happyGoto action_23
action_43 _ = happyFail (happyExpListPerState 43)

action_44 (79) = happyShift action_55
action_44 _ = happyFail (happyExpListPerState 44)

action_45 (66) = happyShift action_26
action_45 (67) = happyShift action_27
action_45 (68) = happyShift action_28
action_45 (84) = happyShift action_52
action_45 (85) = happyShift action_53
action_45 (86) = happyShift action_54
action_45 (12) = happyGoto action_50
action_45 (14) = happyGoto action_51
action_45 _ = happyFail (happyExpListPerState 45)

action_46 (42) = happyShift action_8
action_46 (74) = happyShift action_42
action_46 (77) = happyShift action_43
action_46 (78) = happyShift action_44
action_46 (80) = happyShift action_45
action_46 (82) = happyShift action_46
action_46 (83) = happyShift action_47
action_46 (10) = happyGoto action_49
action_46 _ = happyFail (happyExpListPerState 46)

action_47 (34) = happyShift action_17
action_47 (42) = happyShift action_35
action_47 (54) = happyShift action_36
action_47 (55) = happyShift action_37
action_47 (59) = happyShift action_38
action_47 (61) = happyShift action_39
action_47 (64) = happyShift action_40
action_47 (105) = happyShift action_18
action_47 (106) = happyShift action_19
action_47 (107) = happyShift action_20
action_47 (108) = happyShift action_21
action_47 (16) = happyGoto action_15
action_47 (17) = happyGoto action_33
action_47 (19) = happyGoto action_48
action_47 _ = happyFail (happyExpListPerState 47)

action_48 (62) = happyShift action_66
action_48 (63) = happyShift action_67
action_48 (76) = happyShift action_128
action_48 _ = happyFail (happyExpListPerState 48)

action_49 (63) = happyShift action_58
action_49 _ = happyReduce_13

action_50 _ = happyReduce_34

action_51 (81) = happyShift action_127
action_51 _ = happyFail (happyExpListPerState 51)

action_52 (89) = happyShift action_126
action_52 _ = happyFail (happyExpListPerState 52)

action_53 (105) = happyShift action_123
action_53 (106) = happyShift action_124
action_53 (107) = happyShift action_125
action_53 _ = happyFail (happyExpListPerState 53)

action_54 (110) = happyShift action_122
action_54 _ = happyFail (happyExpListPerState 54)

action_55 (42) = happyShift action_116
action_55 (61) = happyShift action_117
action_55 (66) = happyShift action_26
action_55 (67) = happyShift action_27
action_55 (68) = happyShift action_28
action_55 (84) = happyShift action_52
action_55 (85) = happyShift action_118
action_55 (86) = happyShift action_54
action_55 (93) = happyShift action_119
action_55 (94) = happyShift action_120
action_55 (96) = happyShift action_121
action_55 (12) = happyGoto action_50
action_55 (14) = happyGoto action_114
action_55 (15) = happyGoto action_115
action_55 _ = happyFail (happyExpListPerState 55)

action_56 (57) = happyShift action_113
action_56 (87) = happyShift action_80
action_56 (88) = happyShift action_81
action_56 _ = happyFail (happyExpListPerState 56)

action_57 (75) = happyShift action_112
action_57 _ = happyFail (happyExpListPerState 57)

action_58 (42) = happyShift action_8
action_58 (74) = happyShift action_42
action_58 (77) = happyShift action_43
action_58 (78) = happyShift action_44
action_58 (80) = happyShift action_45
action_58 (82) = happyShift action_46
action_58 (83) = happyShift action_47
action_58 (10) = happyGoto action_111
action_58 _ = happyFail (happyExpListPerState 58)

action_59 (42) = happyShift action_8
action_59 (74) = happyShift action_42
action_59 (77) = happyShift action_43
action_59 (78) = happyShift action_44
action_59 (80) = happyShift action_45
action_59 (82) = happyShift action_46
action_59 (83) = happyShift action_47
action_59 (10) = happyGoto action_110
action_59 _ = happyFail (happyExpListPerState 59)

action_60 (65) = happyShift action_109
action_60 _ = happyFail (happyExpListPerState 60)

action_61 (65) = happyShift action_108
action_61 _ = happyFail (happyExpListPerState 61)

action_62 _ = happyReduce_69

action_63 _ = happyReduce_72

action_64 (42) = happyShift action_24
action_64 (61) = happyShift action_25
action_64 (66) = happyShift action_26
action_64 (67) = happyShift action_27
action_64 (68) = happyShift action_28
action_64 (69) = happyShift action_29
action_64 (70) = happyShift action_30
action_64 (71) = happyShift action_31
action_64 (97) = happyShift action_32
action_64 (11) = happyGoto action_107
action_64 (12) = happyGoto action_23
action_64 _ = happyFail (happyExpListPerState 64)

action_65 (43) = happyShift action_106
action_65 (62) = happyShift action_66
action_65 (63) = happyShift action_67
action_65 _ = happyFail (happyExpListPerState 65)

action_66 (34) = happyShift action_17
action_66 (42) = happyShift action_35
action_66 (54) = happyShift action_36
action_66 (55) = happyShift action_37
action_66 (59) = happyShift action_38
action_66 (61) = happyShift action_39
action_66 (64) = happyShift action_40
action_66 (105) = happyShift action_18
action_66 (106) = happyShift action_19
action_66 (107) = happyShift action_20
action_66 (108) = happyShift action_21
action_66 (16) = happyGoto action_15
action_66 (17) = happyGoto action_33
action_66 (19) = happyGoto action_105
action_66 _ = happyFail (happyExpListPerState 66)

action_67 (34) = happyShift action_17
action_67 (42) = happyShift action_35
action_67 (54) = happyShift action_36
action_67 (55) = happyShift action_37
action_67 (59) = happyShift action_38
action_67 (61) = happyShift action_39
action_67 (64) = happyShift action_40
action_67 (105) = happyShift action_18
action_67 (106) = happyShift action_19
action_67 (107) = happyShift action_20
action_67 (108) = happyShift action_21
action_67 (16) = happyGoto action_15
action_67 (17) = happyGoto action_33
action_67 (19) = happyGoto action_104
action_67 _ = happyFail (happyExpListPerState 67)

action_68 (34) = happyShift action_17
action_68 (105) = happyShift action_18
action_68 (106) = happyShift action_19
action_68 (107) = happyShift action_20
action_68 (108) = happyShift action_21
action_68 (16) = happyGoto action_15
action_68 (17) = happyGoto action_103
action_68 _ = happyFail (happyExpListPerState 68)

action_69 _ = happyReduce_58

action_70 _ = happyReduce_59

action_71 _ = happyReduce_60

action_72 _ = happyReduce_61

action_73 _ = happyReduce_62

action_74 _ = happyReduce_63

action_75 (73) = happyShift action_102
action_75 _ = happyFail (happyExpListPerState 75)

action_76 (34) = happyShift action_17
action_76 (42) = happyShift action_35
action_76 (54) = happyShift action_36
action_76 (55) = happyShift action_37
action_76 (59) = happyShift action_38
action_76 (61) = happyShift action_39
action_76 (64) = happyShift action_40
action_76 (105) = happyShift action_18
action_76 (106) = happyShift action_19
action_76 (107) = happyShift action_20
action_76 (108) = happyShift action_21
action_76 (16) = happyGoto action_15
action_76 (17) = happyGoto action_33
action_76 (19) = happyGoto action_101
action_76 _ = happyFail (happyExpListPerState 76)

action_77 _ = happyReduce_19

action_78 _ = happyReduce_22

action_79 (43) = happyShift action_100
action_79 (87) = happyShift action_80
action_79 (88) = happyShift action_81
action_79 _ = happyFail (happyExpListPerState 79)

action_80 (42) = happyShift action_24
action_80 (61) = happyShift action_25
action_80 (66) = happyShift action_26
action_80 (67) = happyShift action_27
action_80 (68) = happyShift action_28
action_80 (69) = happyShift action_29
action_80 (70) = happyShift action_30
action_80 (71) = happyShift action_31
action_80 (97) = happyShift action_32
action_80 (11) = happyGoto action_99
action_80 (12) = happyGoto action_23
action_80 _ = happyFail (happyExpListPerState 80)

action_81 (42) = happyShift action_24
action_81 (61) = happyShift action_25
action_81 (66) = happyShift action_26
action_81 (67) = happyShift action_27
action_81 (68) = happyShift action_28
action_81 (69) = happyShift action_29
action_81 (70) = happyShift action_30
action_81 (71) = happyShift action_31
action_81 (97) = happyShift action_32
action_81 (11) = happyGoto action_98
action_81 (12) = happyGoto action_23
action_81 _ = happyFail (happyExpListPerState 81)

action_82 (65) = happyShift action_97
action_82 _ = happyFail (happyExpListPerState 82)

action_83 (64) = happyShift action_96
action_83 _ = happyFail (happyExpListPerState 83)

action_84 (42) = happyShift action_24
action_84 (61) = happyShift action_25
action_84 (66) = happyShift action_26
action_84 (67) = happyShift action_27
action_84 (68) = happyShift action_28
action_84 (69) = happyShift action_29
action_84 (70) = happyShift action_30
action_84 (71) = happyShift action_31
action_84 (97) = happyShift action_32
action_84 (11) = happyGoto action_95
action_84 (12) = happyGoto action_23
action_84 _ = happyFail (happyExpListPerState 84)

action_85 (110) = happyShift action_94
action_85 _ = happyFail (happyExpListPerState 85)

action_86 (110) = happyShift action_93
action_86 _ = happyFail (happyExpListPerState 86)

action_87 (36) = happyShift action_92
action_87 (31) = happyGoto action_90
action_87 (32) = happyGoto action_91
action_87 _ = happyFail (happyExpListPerState 87)

action_88 (43) = happyShift action_89
action_88 (63) = happyShift action_58
action_88 (76) = happyShift action_59
action_88 _ = happyFail (happyExpListPerState 88)

action_89 _ = happyReduce_7

action_90 (100) = happyShift action_157
action_90 _ = happyFail (happyExpListPerState 90)

action_91 (110) = happyShift action_156
action_91 (33) = happyGoto action_155
action_91 _ = happyFail (happyExpListPerState 91)

action_92 _ = happyReduce_93

action_93 (95) = happyShift action_154
action_93 _ = happyFail (happyExpListPerState 93)

action_94 (47) = happyShift action_153
action_94 _ = happyFail (happyExpListPerState 94)

action_95 (98) = happyShift action_152
action_95 _ = happyReduce_54

action_96 (66) = happyShift action_26
action_96 (67) = happyShift action_27
action_96 (68) = happyShift action_28
action_96 (12) = happyGoto action_151
action_96 _ = happyFail (happyExpListPerState 96)

action_97 (42) = happyShift action_24
action_97 (61) = happyShift action_25
action_97 (66) = happyShift action_26
action_97 (67) = happyShift action_27
action_97 (68) = happyShift action_28
action_97 (69) = happyShift action_29
action_97 (70) = happyShift action_30
action_97 (71) = happyShift action_31
action_97 (97) = happyShift action_32
action_97 (11) = happyGoto action_150
action_97 (12) = happyGoto action_23
action_97 _ = happyFail (happyExpListPerState 97)

action_98 (88) = happyShift action_81
action_98 _ = happyReduce_23

action_99 (87) = happyShift action_80
action_99 (88) = happyShift action_81
action_99 _ = happyReduce_24

action_100 _ = happyReduce_16

action_101 (62) = happyShift action_66
action_101 (63) = happyShift action_67
action_101 _ = happyReduce_20

action_102 (61) = happyShift action_146
action_102 (72) = happyShift action_147
action_102 (92) = happyShift action_148
action_102 (95) = happyShift action_149
action_102 (13) = happyGoto action_145
action_102 _ = happyFail (happyExpListPerState 102)

action_103 _ = happyReduce_68

action_104 (62) = happyShift action_66
action_104 (63) = happyShift action_67
action_104 _ = happyReduce_71

action_105 (62) = happyShift action_66
action_105 _ = happyReduce_70

action_106 _ = happyReduce_64

action_107 (56) = happyShift action_143
action_107 (57) = happyShift action_144
action_107 (87) = happyShift action_80
action_107 (88) = happyShift action_81
action_107 _ = happyFail (happyExpListPerState 107)

action_108 (42) = happyShift action_24
action_108 (61) = happyShift action_25
action_108 (66) = happyShift action_26
action_108 (67) = happyShift action_27
action_108 (68) = happyShift action_28
action_108 (69) = happyShift action_29
action_108 (70) = happyShift action_30
action_108 (71) = happyShift action_31
action_108 (97) = happyShift action_32
action_108 (11) = happyGoto action_142
action_108 (12) = happyGoto action_23
action_108 _ = happyFail (happyExpListPerState 108)

action_109 (42) = happyShift action_24
action_109 (61) = happyShift action_25
action_109 (66) = happyShift action_26
action_109 (67) = happyShift action_27
action_109 (68) = happyShift action_28
action_109 (69) = happyShift action_29
action_109 (70) = happyShift action_30
action_109 (71) = happyShift action_31
action_109 (97) = happyShift action_32
action_109 (11) = happyGoto action_141
action_109 (12) = happyGoto action_23
action_109 _ = happyFail (happyExpListPerState 109)

action_110 (63) = happyShift action_58
action_110 (76) = happyShift action_59
action_110 _ = happyReduce_15

action_111 (63) = happyShift action_58
action_111 _ = happyReduce_14

action_112 (42) = happyShift action_116
action_112 (61) = happyShift action_117
action_112 (66) = happyShift action_26
action_112 (67) = happyShift action_27
action_112 (68) = happyShift action_28
action_112 (84) = happyShift action_52
action_112 (85) = happyShift action_118
action_112 (86) = happyShift action_54
action_112 (93) = happyShift action_119
action_112 (94) = happyShift action_120
action_112 (96) = happyShift action_121
action_112 (12) = happyGoto action_50
action_112 (14) = happyGoto action_114
action_112 (15) = happyGoto action_140
action_112 _ = happyFail (happyExpListPerState 112)

action_113 (58) = happyShift action_139
action_113 _ = happyFail (happyExpListPerState 113)

action_114 _ = happyReduce_41

action_115 (87) = happyShift action_137
action_115 (88) = happyShift action_138
action_115 _ = happyReduce_10

action_116 (42) = happyShift action_116
action_116 (61) = happyShift action_117
action_116 (66) = happyShift action_26
action_116 (67) = happyShift action_27
action_116 (68) = happyShift action_28
action_116 (84) = happyShift action_52
action_116 (85) = happyShift action_118
action_116 (86) = happyShift action_54
action_116 (93) = happyShift action_119
action_116 (94) = happyShift action_120
action_116 (96) = happyShift action_121
action_116 (12) = happyGoto action_50
action_116 (14) = happyGoto action_114
action_116 (15) = happyGoto action_136
action_116 _ = happyFail (happyExpListPerState 116)

action_117 (42) = happyShift action_116
action_117 (61) = happyShift action_117
action_117 (66) = happyShift action_26
action_117 (67) = happyShift action_27
action_117 (68) = happyShift action_28
action_117 (84) = happyShift action_52
action_117 (85) = happyShift action_118
action_117 (86) = happyShift action_54
action_117 (93) = happyShift action_119
action_117 (94) = happyShift action_120
action_117 (96) = happyShift action_121
action_117 (12) = happyGoto action_50
action_117 (14) = happyGoto action_114
action_117 (15) = happyGoto action_135
action_117 _ = happyFail (happyExpListPerState 117)

action_118 (93) = happyShift action_134
action_118 (105) = happyShift action_123
action_118 (106) = happyShift action_124
action_118 (107) = happyShift action_125
action_118 _ = happyFail (happyExpListPerState 118)

action_119 (65) = happyShift action_133
action_119 _ = happyFail (happyExpListPerState 119)

action_120 (93) = happyShift action_132
action_120 _ = happyFail (happyExpListPerState 120)

action_121 (84) = happyShift action_131
action_121 _ = happyFail (happyExpListPerState 121)

action_122 _ = happyReduce_38

action_123 _ = happyReduce_35

action_124 _ = happyReduce_36

action_125 _ = happyReduce_37

action_126 _ = happyReduce_39

action_127 (66) = happyShift action_26
action_127 (67) = happyShift action_27
action_127 (68) = happyShift action_28
action_127 (84) = happyShift action_52
action_127 (85) = happyShift action_53
action_127 (86) = happyShift action_54
action_127 (12) = happyGoto action_50
action_127 (14) = happyGoto action_130
action_127 _ = happyFail (happyExpListPerState 127)

action_128 (42) = happyShift action_8
action_128 (74) = happyShift action_42
action_128 (77) = happyShift action_43
action_128 (78) = happyShift action_44
action_128 (80) = happyShift action_45
action_128 (82) = happyShift action_46
action_128 (83) = happyShift action_47
action_128 (10) = happyGoto action_129
action_128 _ = happyFail (happyExpListPerState 128)

action_129 (63) = happyShift action_58
action_129 (76) = happyShift action_59
action_129 _ = happyReduce_12

action_130 _ = happyReduce_11

action_131 (105) = happyShift action_18
action_131 (106) = happyShift action_19
action_131 (107) = happyShift action_20
action_131 (108) = happyShift action_21
action_131 (16) = happyGoto action_175
action_131 _ = happyFail (happyExpListPerState 131)

action_132 _ = happyReduce_45

action_133 (42) = happyShift action_24
action_133 (61) = happyShift action_25
action_133 (66) = happyShift action_26
action_133 (67) = happyShift action_27
action_133 (68) = happyShift action_28
action_133 (69) = happyShift action_29
action_133 (70) = happyShift action_30
action_133 (71) = happyShift action_31
action_133 (97) = happyShift action_32
action_133 (11) = happyGoto action_174
action_133 (12) = happyGoto action_23
action_133 _ = happyFail (happyExpListPerState 133)

action_134 _ = happyReduce_44

action_135 _ = happyReduce_46

action_136 (43) = happyShift action_173
action_136 (87) = happyShift action_137
action_136 (88) = happyShift action_138
action_136 _ = happyFail (happyExpListPerState 136)

action_137 (42) = happyShift action_116
action_137 (61) = happyShift action_117
action_137 (66) = happyShift action_26
action_137 (67) = happyShift action_27
action_137 (68) = happyShift action_28
action_137 (84) = happyShift action_52
action_137 (85) = happyShift action_118
action_137 (86) = happyShift action_54
action_137 (93) = happyShift action_119
action_137 (94) = happyShift action_120
action_137 (96) = happyShift action_121
action_137 (12) = happyGoto action_50
action_137 (14) = happyGoto action_114
action_137 (15) = happyGoto action_172
action_137 _ = happyFail (happyExpListPerState 137)

action_138 (42) = happyShift action_116
action_138 (61) = happyShift action_117
action_138 (66) = happyShift action_26
action_138 (67) = happyShift action_27
action_138 (68) = happyShift action_28
action_138 (84) = happyShift action_52
action_138 (85) = happyShift action_118
action_138 (86) = happyShift action_54
action_138 (93) = happyShift action_119
action_138 (94) = happyShift action_120
action_138 (96) = happyShift action_121
action_138 (12) = happyGoto action_50
action_138 (14) = happyGoto action_114
action_138 (15) = happyGoto action_171
action_138 _ = happyFail (happyExpListPerState 138)

action_139 _ = happyReduce_9

action_140 (87) = happyShift action_137
action_140 (88) = happyShift action_138
action_140 (90) = happyShift action_170
action_140 _ = happyFail (happyExpListPerState 140)

action_141 (45) = happyShift action_169
action_141 (87) = happyShift action_80
action_141 (88) = happyShift action_81
action_141 _ = happyFail (happyExpListPerState 141)

action_142 (45) = happyShift action_168
action_142 (87) = happyShift action_80
action_142 (88) = happyShift action_81
action_142 _ = happyFail (happyExpListPerState 142)

action_143 _ = happyReduce_73

action_144 (58) = happyShift action_167
action_144 _ = happyFail (happyExpListPerState 144)

action_145 _ = happyReduce_21

action_146 (61) = happyShift action_146
action_146 (72) = happyShift action_147
action_146 (92) = happyShift action_148
action_146 (95) = happyShift action_149
action_146 (13) = happyGoto action_166
action_146 _ = happyFail (happyExpListPerState 146)

action_147 (110) = happyShift action_165
action_147 _ = happyFail (happyExpListPerState 147)

action_148 (110) = happyShift action_164
action_148 _ = happyFail (happyExpListPerState 148)

action_149 (110) = happyShift action_163
action_149 _ = happyFail (happyExpListPerState 149)

action_150 _ = happyReduce_56

action_151 _ = happyReduce_57

action_152 (60) = happyShift action_162
action_152 _ = happyFail (happyExpListPerState 152)

action_153 (45) = happyShift action_161
action_153 _ = happyFail (happyExpListPerState 153)

action_154 (45) = happyShift action_160
action_154 _ = happyFail (happyExpListPerState 154)

action_155 _ = happyReduce_92

action_156 (36) = happyShift action_159
action_156 _ = happyReduce_94

action_157 (45) = happyShift action_158
action_157 _ = happyFail (happyExpListPerState 157)

action_158 (110) = happyShift action_185
action_158 (26) = happyGoto action_183
action_158 (27) = happyGoto action_184
action_158 _ = happyReduce_86

action_159 (110) = happyShift action_156
action_159 (33) = happyGoto action_182
action_159 _ = happyFail (happyExpListPerState 159)

action_160 (110) = happyShift action_181
action_160 _ = happyFail (happyExpListPerState 160)

action_161 (110) = happyShift action_180
action_161 _ = happyFail (happyExpListPerState 161)

action_162 (64) = happyShift action_179
action_162 _ = happyFail (happyExpListPerState 162)

action_163 _ = happyReduce_31

action_164 _ = happyReduce_30

action_165 _ = happyReduce_32

action_166 _ = happyReduce_33

action_167 _ = happyReduce_74

action_168 (34) = happyShift action_17
action_168 (42) = happyShift action_35
action_168 (54) = happyShift action_36
action_168 (55) = happyShift action_37
action_168 (59) = happyShift action_38
action_168 (61) = happyShift action_39
action_168 (64) = happyShift action_40
action_168 (105) = happyShift action_18
action_168 (106) = happyShift action_19
action_168 (107) = happyShift action_20
action_168 (108) = happyShift action_21
action_168 (16) = happyGoto action_15
action_168 (17) = happyGoto action_33
action_168 (19) = happyGoto action_178
action_168 _ = happyFail (happyExpListPerState 168)

action_169 (34) = happyShift action_17
action_169 (42) = happyShift action_35
action_169 (54) = happyShift action_36
action_169 (55) = happyShift action_37
action_169 (59) = happyShift action_38
action_169 (61) = happyShift action_39
action_169 (64) = happyShift action_40
action_169 (105) = happyShift action_18
action_169 (106) = happyShift action_19
action_169 (107) = happyShift action_20
action_169 (108) = happyShift action_21
action_169 (16) = happyGoto action_15
action_169 (17) = happyGoto action_33
action_169 (19) = happyGoto action_177
action_169 _ = happyFail (happyExpListPerState 169)

action_170 (91) = happyShift action_176
action_170 _ = happyFail (happyExpListPerState 170)

action_171 (88) = happyShift action_138
action_171 _ = happyReduce_47

action_172 (87) = happyShift action_137
action_172 (88) = happyShift action_138
action_172 _ = happyReduce_48

action_173 _ = happyReduce_40

action_174 _ = happyReduce_42

action_175 _ = happyReduce_43

action_176 (42) = happyShift action_116
action_176 (61) = happyShift action_117
action_176 (66) = happyShift action_26
action_176 (67) = happyShift action_27
action_176 (68) = happyShift action_28
action_176 (84) = happyShift action_52
action_176 (85) = happyShift action_118
action_176 (86) = happyShift action_54
action_176 (93) = happyShift action_119
action_176 (94) = happyShift action_120
action_176 (96) = happyShift action_121
action_176 (12) = happyGoto action_50
action_176 (14) = happyGoto action_114
action_176 (15) = happyGoto action_193
action_176 _ = happyFail (happyExpListPerState 176)

action_177 (62) = happyShift action_66
action_177 (63) = happyShift action_67
action_177 _ = happyReduce_67

action_178 (62) = happyShift action_66
action_178 (63) = happyShift action_67
action_178 _ = happyReduce_66

action_179 (66) = happyShift action_26
action_179 (67) = happyShift action_27
action_179 (68) = happyShift action_28
action_179 (12) = happyGoto action_192
action_179 _ = happyFail (happyExpListPerState 179)

action_180 (48) = happyShift action_191
action_180 _ = happyFail (happyExpListPerState 180)

action_181 (51) = happyShift action_190
action_181 (28) = happyGoto action_188
action_181 (29) = happyGoto action_189
action_181 _ = happyReduce_88

action_182 _ = happyReduce_95

action_183 (101) = happyShift action_187
action_183 _ = happyFail (happyExpListPerState 183)

action_184 _ = happyReduce_85

action_185 (110) = happyShift action_185
action_185 (27) = happyGoto action_186
action_185 _ = happyReduce_86

action_186 _ = happyReduce_87

action_187 (45) = happyShift action_200
action_187 _ = happyFail (happyExpListPerState 187)

action_188 (52) = happyShift action_199
action_188 (23) = happyGoto action_198
action_188 _ = happyReduce_79

action_189 (51) = happyShift action_190
action_189 (28) = happyGoto action_197
action_189 (29) = happyGoto action_189
action_189 _ = happyReduce_88

action_190 (45) = happyShift action_196
action_190 _ = happyFail (happyExpListPerState 190)

action_191 (45) = happyShift action_195
action_191 _ = happyFail (happyExpListPerState 191)

action_192 _ = happyReduce_55

action_193 (76) = happyShift action_194
action_193 (87) = happyShift action_137
action_193 (88) = happyShift action_138
action_193 _ = happyFail (happyExpListPerState 193)

action_194 (42) = happyShift action_8
action_194 (74) = happyShift action_42
action_194 (77) = happyShift action_43
action_194 (78) = happyShift action_44
action_194 (80) = happyShift action_45
action_194 (82) = happyShift action_46
action_194 (83) = happyShift action_47
action_194 (10) = happyGoto action_209
action_194 _ = happyFail (happyExpListPerState 194)

action_195 (105) = happyShift action_18
action_195 (106) = happyShift action_19
action_195 (107) = happyShift action_20
action_195 (108) = happyShift action_21
action_195 (109) = happyShift action_208
action_195 (16) = happyGoto action_206
action_195 (21) = happyGoto action_207
action_195 _ = happyFail (happyExpListPerState 195)

action_196 (110) = happyShift action_205
action_196 _ = happyFail (happyExpListPerState 196)

action_197 _ = happyReduce_89

action_198 (53) = happyShift action_204
action_198 (24) = happyGoto action_203
action_198 _ = happyReduce_81

action_199 (45) = happyShift action_202
action_199 _ = happyFail (happyExpListPerState 199)

action_200 (110) = happyShift action_185
action_200 (26) = happyGoto action_201
action_200 (27) = happyGoto action_184
action_200 _ = happyReduce_86

action_201 (102) = happyShift action_216
action_201 _ = happyFail (happyExpListPerState 201)

action_202 (110) = happyShift action_185
action_202 (26) = happyGoto action_215
action_202 (27) = happyGoto action_184
action_202 _ = happyReduce_86

action_203 (103) = happyShift action_214
action_203 (25) = happyGoto action_213
action_203 _ = happyReduce_83

action_204 (45) = happyShift action_212
action_204 _ = happyFail (happyExpListPerState 204)

action_205 (44) = happyShift action_211
action_205 _ = happyFail (happyExpListPerState 205)

action_206 _ = happyReduce_76

action_207 (49) = happyShift action_210
action_207 _ = happyFail (happyExpListPerState 207)

action_208 _ = happyReduce_77

action_209 (63) = happyShift action_58
action_209 (76) = happyShift action_59
action_209 _ = happyReduce_8

action_210 (45) = happyShift action_221
action_210 _ = happyFail (happyExpListPerState 210)

action_211 (110) = happyShift action_220
action_211 _ = happyFail (happyExpListPerState 211)

action_212 (110) = happyShift action_185
action_212 (26) = happyGoto action_219
action_212 (27) = happyGoto action_184
action_212 _ = happyReduce_86

action_213 _ = happyReduce_78

action_214 (102) = happyShift action_218
action_214 _ = happyFail (happyExpListPerState 214)

action_215 _ = happyReduce_80

action_216 (45) = happyShift action_217
action_216 _ = happyFail (happyExpListPerState 216)

action_217 (110) = happyShift action_185
action_217 (27) = happyGoto action_224
action_217 _ = happyReduce_86

action_218 (45) = happyShift action_223
action_218 _ = happyFail (happyExpListPerState 218)

action_219 _ = happyReduce_82

action_220 _ = happyReduce_90

action_221 (108) = happyShift action_222
action_221 _ = happyFail (happyExpListPerState 221)

action_222 (50) = happyShift action_226
action_222 _ = happyFail (happyExpListPerState 222)

action_223 (110) = happyShift action_185
action_223 (26) = happyGoto action_225
action_223 (27) = happyGoto action_184
action_223 _ = happyReduce_86

action_224 _ = happyReduce_91

action_225 _ = happyReduce_84

action_226 (45) = happyShift action_227
action_226 _ = happyFail (happyExpListPerState 226)

action_227 (42) = happyShift action_8
action_227 (74) = happyShift action_42
action_227 (77) = happyShift action_43
action_227 (78) = happyShift action_44
action_227 (80) = happyShift action_45
action_227 (82) = happyShift action_46
action_227 (83) = happyShift action_47
action_227 (10) = happyGoto action_228
action_227 _ = happyFail (happyExpListPerState 227)

action_228 (63) = happyShift action_58
action_228 (76) = happyShift action_59
action_228 _ = happyReduce_75

happyReduce_7 = happySpecReduce_3  10 happyReduction_7
happyReduction_7 _
	(HappyAbsSyn10  happy_var_2)
	_
	 =  HappyAbsSyn10
		 (happy_var_2
	)
happyReduction_7 _ _ _  = notHappyAtAll 

happyReduce_8 = happyReduce 9 10 happyReduction_8
happyReduction_8 ((HappyAbsSyn10  happy_var_9) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn15  happy_var_7) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn15  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn16  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn10
		 (AndThen (ChooseNFrom happy_var_2 happy_var_4 happy_var_7) happy_var_9
	) `HappyStk` happyRest

happyReduce_9 = happyReduce 4 10 happyReduction_9
happyReduction_9 (_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn11  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn10
		 (KnowInPlay happy_var_2
	) `HappyStk` happyRest

happyReduce_10 = happySpecReduce_3  10 happyReduction_10
happyReduction_10 (HappyAbsSyn15  happy_var_3)
	_
	_
	 =  HappyAbsSyn10
		 (LookAt happy_var_3
	)
happyReduction_10 _ _ _  = notHappyAtAll 

happyReduce_11 = happyReduce 4 10 happyReduction_11
happyReduction_11 ((HappyAbsSyn14  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn14  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn10
		 (SwapWith happy_var_2 happy_var_4
	) `HappyStk` happyRest

happyReduce_12 = happyReduce 4 10 happyReduction_12
happyReduction_12 ((HappyAbsSyn10  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn13  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn10
		 (IfThen happy_var_2 happy_var_4
	) `HappyStk` happyRest

happyReduce_13 = happySpecReduce_2  10 happyReduction_13
happyReduction_13 (HappyAbsSyn10  happy_var_2)
	_
	 =  HappyAbsSyn10
		 (ActionOr NoOp happy_var_2
	)
happyReduction_13 _ _  = notHappyAtAll 

happyReduce_14 = happySpecReduce_3  10 happyReduction_14
happyReduction_14 (HappyAbsSyn10  happy_var_3)
	_
	(HappyAbsSyn10  happy_var_1)
	 =  HappyAbsSyn10
		 (ActionOr happy_var_1 happy_var_3
	)
happyReduction_14 _ _ _  = notHappyAtAll 

happyReduce_15 = happySpecReduce_3  10 happyReduction_15
happyReduction_15 (HappyAbsSyn10  happy_var_3)
	_
	(HappyAbsSyn10  happy_var_1)
	 =  HappyAbsSyn10
		 (AndThen happy_var_1 happy_var_3
	)
happyReduction_15 _ _ _  = notHappyAtAll 

happyReduce_16 = happySpecReduce_3  11 happyReduction_16
happyReduction_16 _
	(HappyAbsSyn11  happy_var_2)
	_
	 =  HappyAbsSyn11
		 (happy_var_2
	)
happyReduction_16 _ _ _  = notHappyAtAll 

happyReduce_17 = happySpecReduce_1  11 happyReduction_17
happyReduction_17 (HappyAbsSyn12  happy_var_1)
	 =  HappyAbsSyn11
		 (FromPlayerSpecs $ Set.singleton happy_var_1
	)
happyReduction_17 _  = notHappyAtAll 

happyReduce_18 = happySpecReduce_1  11 happyReduction_18
happyReduction_18 _
	 =  HappyAbsSyn11
		 (everyone
	)

happyReduce_19 = happySpecReduce_2  11 happyReduction_19
happyReduction_19 _
	_
	 =  HappyAbsSyn11
		 (PlayersSatisfying Awake
	)

happyReduce_20 = happySpecReduce_3  11 happyReduction_20
happyReduction_20 (HappyAbsSyn13  happy_var_3)
	_
	_
	 =  HappyAbsSyn11
		 (PlayersSatisfying happy_var_3
	)
happyReduction_20 _ _ _  = notHappyAtAll 

happyReduce_21 = happyReduce 4 11 happyReduction_21
happyReduction_21 ((HappyAbsSyn13  happy_var_4) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn11
		 (PlayersSatisfying happy_var_4
	) `HappyStk` happyRest

happyReduce_22 = happySpecReduce_2  11 happyReduction_22
happyReduction_22 (HappyAbsSyn11  happy_var_2)
	_
	 =  HappyAbsSyn11
		 (PSDifference everyone happy_var_2
	)
happyReduction_22 _ _  = notHappyAtAll 

happyReduce_23 = happySpecReduce_3  11 happyReduction_23
happyReduction_23 (HappyAbsSyn11  happy_var_3)
	_
	(HappyAbsSyn11  happy_var_1)
	 =  HappyAbsSyn11
		 (PSIntersection happy_var_1 happy_var_3
	)
happyReduction_23 _ _ _  = notHappyAtAll 

happyReduce_24 = happySpecReduce_3  11 happyReduction_24
happyReduction_24 (HappyAbsSyn11  happy_var_3)
	_
	(HappyAbsSyn11  happy_var_1)
	 =  HappyAbsSyn11
		 (PSUnion happy_var_1 happy_var_3
	)
happyReduction_24 _ _ _  = notHappyAtAll 

happyReduce_25 = happyMonadReduce 1 11 happyReduction_25
happyReduction_25 (_ `HappyStk`
	happyRest) tk
	 = happyThen ((( parsePlayerSet $ lexer "left union right"))
	) (\r -> happyReturn (HappyAbsSyn11 r))

happyReduce_26 = happyMonadReduce 1 11 happyReduction_26
happyReduction_26 (_ `HappyStk`
	happyRest) tk
	 = happyThen ((( parsePlayerSet $ lexer "not self"))
	) (\r -> happyReturn (HappyAbsSyn11 r))

happyReduce_27 = happySpecReduce_1  12 happyReduction_27
happyReduction_27 _
	 =  HappyAbsSyn12
		 (Self
	)

happyReduce_28 = happySpecReduce_1  12 happyReduction_28
happyReduction_28 _
	 =  HappyAbsSyn12
		 (PSLeft
	)

happyReduce_29 = happySpecReduce_1  12 happyReduction_29
happyReduction_29 _
	 =  HappyAbsSyn12
		 (PSRight
	)

happyReduce_30 = happySpecReduce_2  13 happyReduction_30
happyReduction_30 (HappyTerminal (TokenName happy_var_2))
	_
	 =  HappyAbsSyn13
		 (RoleNameIs happy_var_2
	)
happyReduction_30 _ _  = notHappyAtAll 

happyReduce_31 = happySpecReduce_2  13 happyReduction_31
happyReduction_31 (HappyTerminal (TokenName happy_var_2))
	_
	 =  HappyAbsSyn13
		 (RoleRaceIs happy_var_2
	)
happyReduction_31 _ _  = notHappyAtAll 

happyReduce_32 = happySpecReduce_2  13 happyReduction_32
happyReduction_32 (HappyTerminal (TokenName happy_var_2))
	_
	 =  HappyAbsSyn13
		 (RoleTeamIs happy_var_2
	)
happyReduction_32 _ _  = notHappyAtAll 

happyReduce_33 = happySpecReduce_2  13 happyReduction_33
happyReduction_33 (HappyAbsSyn13  happy_var_2)
	_
	 =  HappyAbsSyn13
		 (Not happy_var_2
	)
happyReduction_33 _ _  = notHappyAtAll 

happyReduce_34 = happySpecReduce_1  14 happyReduction_34
happyReduction_34 (HappyAbsSyn12  happy_var_1)
	 =  HappyAbsSyn14
		 (PlayerRelative happy_var_1
	)
happyReduction_34 _  = notHappyAtAll 

happyReduce_35 = happySpecReduce_2  14 happyReduction_35
happyReduction_35 _
	_
	 =  HappyAbsSyn14
		 (GlobalCP Center1
	)

happyReduce_36 = happySpecReduce_2  14 happyReduction_36
happyReduction_36 _
	_
	 =  HappyAbsSyn14
		 (GlobalCP Center2
	)

happyReduce_37 = happySpecReduce_2  14 happyReduction_37
happyReduction_37 _
	_
	 =  HappyAbsSyn14
		 (GlobalCP Center3
	)

happyReduce_38 = happySpecReduce_2  14 happyReduction_38
happyReduction_38 (HappyTerminal (TokenName happy_var_2))
	_
	 =  HappyAbsSyn14
		 (GlobalCP $ Extra happy_var_2
	)
happyReduction_38 _ _  = notHappyAtAll 

happyReduce_39 = happySpecReduce_2  14 happyReduction_39
happyReduction_39 _
	_
	 =  HappyAbsSyn14
		 (LastChosen
	)

happyReduce_40 = happySpecReduce_3  15 happyReduction_40
happyReduction_40 _
	(HappyAbsSyn15  happy_var_2)
	_
	 =  HappyAbsSyn15
		 (happy_var_2
	)
happyReduction_40 _ _ _  = notHappyAtAll 

happyReduce_41 = happySpecReduce_1  15 happyReduction_41
happyReduction_41 (HappyAbsSyn14  happy_var_1)
	 =  HappyAbsSyn15
		 (FromCardPosSpecs $ Set.singleton happy_var_1
	)
happyReduction_41 _  = notHappyAtAll 

happyReduce_42 = happySpecReduce_3  15 happyReduction_42
happyReduction_42 (HappyAbsSyn11  happy_var_3)
	_
	_
	 =  HappyAbsSyn15
		 (OfPlayers happy_var_3
	)
happyReduction_42 _ _ _  = notHappyAtAll 

happyReduce_43 = happySpecReduce_3  15 happyReduction_43
happyReduction_43 (HappyAbsSyn16  happy_var_3)
	_
	_
	 =  HappyAbsSyn15
		 (LastNChosen happy_var_3
	)
happyReduction_43 _ _ _  = notHappyAtAll 

happyReduce_44 = happySpecReduce_2  15 happyReduction_44
happyReduction_44 _
	_
	 =  HappyAbsSyn15
		 (FromCardPosSpecs $             
             Set.fromList $
             GlobalCP `fmap`
             [ Center1, Center2, Center3 ]
	)

happyReduce_45 = happySpecReduce_2  15 happyReduction_45
happyReduction_45 _
	_
	 =  HappyAbsSyn15
		 (AllCards
	)

happyReduce_46 = happySpecReduce_2  15 happyReduction_46
happyReduction_46 (HappyAbsSyn15  happy_var_2)
	_
	 =  HappyAbsSyn15
		 (CSDifference AllCards happy_var_2
	)
happyReduction_46 _ _  = notHappyAtAll 

happyReduce_47 = happySpecReduce_3  15 happyReduction_47
happyReduction_47 (HappyAbsSyn15  happy_var_3)
	_
	(HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn15
		 (CSIntersection happy_var_1 happy_var_3
	)
happyReduction_47 _ _ _  = notHappyAtAll 

happyReduce_48 = happySpecReduce_3  15 happyReduction_48
happyReduction_48 (HappyAbsSyn15  happy_var_3)
	_
	(HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn15
		 (CSUnion happy_var_1 happy_var_3
	)
happyReduction_48 _ _ _  = notHappyAtAll 

happyReduce_49 = happySpecReduce_1  16 happyReduction_49
happyReduction_49 _
	 =  HappyAbsSyn16
		 (1
	)

happyReduce_50 = happySpecReduce_1  16 happyReduction_50
happyReduction_50 _
	 =  HappyAbsSyn16
		 (2
	)

happyReduce_51 = happySpecReduce_1  16 happyReduction_51
happyReduction_51 _
	 =  HappyAbsSyn16
		 (3
	)

happyReduce_52 = happySpecReduce_1  16 happyReduction_52
happyReduction_52 (HappyTerminal (TokenInt happy_var_1))
	 =  HappyAbsSyn16
		 (happy_var_1
	)
happyReduction_52 _  = notHappyAtAll 

happyReduce_53 = happySpecReduce_1  17 happyReduction_53
happyReduction_53 (HappyAbsSyn16  happy_var_1)
	 =  HappyAbsSyn17
		 (GlobalN happy_var_1
	)
happyReduction_53 _  = notHappyAtAll 

happyReduce_54 = happySpecReduce_3  17 happyReduction_54
happyReduction_54 (HappyAbsSyn11  happy_var_3)
	_
	_
	 =  HappyAbsSyn17
		 (SizePlayerSet happy_var_3
	)
happyReduction_54 _ _ _  = notHappyAtAll 

happyReduce_55 = happyReduce 7 17 happyReduction_55
happyReduction_55 ((HappyAbsSyn12  happy_var_7) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn11  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn17
		 (SizePlayerSet $ PSIntersection happy_var_3 $ PlayersSatisfying $ VotedFor happy_var_7
	) `HappyStk` happyRest

happyReduce_56 = happyReduce 4 17 happyReduction_56
happyReduction_56 ((HappyAbsSyn11  happy_var_4) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn17
		 (SizePlayerSet $ PSIntersection happy_var_4 $ PlayersSatisfying Dead
	) `HappyStk` happyRest

happyReduce_57 = happyReduce 4 17 happyReduction_57
happyReduction_57 ((HappyAbsSyn12  happy_var_4) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn17
		 (SizePlayerSet $ PlayersSatisfying $ VotedFor happy_var_4
	) `HappyStk` happyRest

happyReduce_58 = happySpecReduce_1  18 happyReduction_58
happyReduction_58 _
	 =  HappyAbsSyn18
		 (OpEq
	)

happyReduce_59 = happySpecReduce_1  18 happyReduction_59
happyReduction_59 _
	 =  HappyAbsSyn18
		 (OpNeq
	)

happyReduce_60 = happySpecReduce_1  18 happyReduction_60
happyReduction_60 _
	 =  HappyAbsSyn18
		 (OpLt
	)

happyReduce_61 = happySpecReduce_1  18 happyReduction_61
happyReduction_61 _
	 =  HappyAbsSyn18
		 (OpGt
	)

happyReduce_62 = happySpecReduce_1  18 happyReduction_62
happyReduction_62 _
	 =  HappyAbsSyn18
		 (OpLeq
	)

happyReduce_63 = happySpecReduce_1  18 happyReduction_63
happyReduction_63 _
	 =  HappyAbsSyn18
		 (OpGeq
	)

happyReduce_64 = happySpecReduce_3  19 happyReduction_64
happyReduction_64 _
	(HappyAbsSyn13  happy_var_2)
	_
	 =  HappyAbsSyn13
		 (happy_var_2
	)
happyReduction_64 _ _ _  = notHappyAtAll 

happyReduce_65 = happySpecReduce_1  19 happyReduction_65
happyReduction_65 _
	 =  HappyAbsSyn13
		 (DefaultDeath
	)

happyReduce_66 = happyReduce 6 19 happyReduction_66
happyReduction_66 ((HappyAbsSyn13  happy_var_6) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn11  happy_var_4) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn13
		 (AllOf happy_var_4 happy_var_6
	) `HappyStk` happyRest

happyReduce_67 = happyReduce 6 19 happyReduction_67
happyReduction_67 ((HappyAbsSyn13  happy_var_6) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn11  happy_var_4) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn13
		 (AnyOf happy_var_4 happy_var_6
	) `HappyStk` happyRest

happyReduce_68 = happySpecReduce_3  19 happyReduction_68
happyReduction_68 (HappyAbsSyn17  happy_var_3)
	(HappyAbsSyn18  happy_var_2)
	(HappyAbsSyn17  happy_var_1)
	 =  HappyAbsSyn13
		 (Numeric happy_var_1 happy_var_2 happy_var_3
	)
happyReduction_68 _ _ _  = notHappyAtAll 

happyReduce_69 = happySpecReduce_2  19 happyReduction_69
happyReduction_69 (HappyAbsSyn13  happy_var_2)
	_
	 =  HappyAbsSyn13
		 (Not happy_var_2
	)
happyReduction_69 _ _  = notHappyAtAll 

happyReduce_70 = happySpecReduce_3  19 happyReduction_70
happyReduction_70 (HappyAbsSyn13  happy_var_3)
	_
	(HappyAbsSyn13  happy_var_1)
	 =  HappyAbsSyn13
		 (And happy_var_1 happy_var_3
	)
happyReduction_70 _ _ _  = notHappyAtAll 

happyReduce_71 = happySpecReduce_3  19 happyReduction_71
happyReduction_71 (HappyAbsSyn13  happy_var_3)
	_
	(HappyAbsSyn13  happy_var_1)
	 =  HappyAbsSyn13
		 (Or happy_var_1 happy_var_3
	)
happyReduction_71 _ _ _  = notHappyAtAll 

happyReduce_72 = happySpecReduce_2  19 happyReduction_72
happyReduction_72 _
	_
	 =  HappyAbsSyn13
		 (MostVoted
	)

happyReduce_73 = happyReduce 4 19 happyReduction_73
happyReduction_73 (_ `HappyStk`
	(HappyAbsSyn11  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn13
		 (AnyOf happy_var_3 Dead
	) `HappyStk` happyRest

happyReduce_74 = happyReduce 5 19 happyReduction_74
happyReduction_74 (_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn11  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn13
		 (Numeric (SizePlayerSet happy_var_3) OpGt (GlobalN 0)
	) `HappyStk` happyRest

happyReduce_75 = happyReduce 15 20 happyReduction_75
happyReduction_75 ((HappyAbsSyn10  happy_var_15) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TokenInt happy_var_12)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn21  happy_var_9) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TokenName happy_var_6)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TokenName happy_var_3)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn20
		 (\phaseName -> Setup { _phaseName = phaseName
                                  , _beginPrompt = happy_var_3
                                  , _finishPrompt = happy_var_6
                                  , _order = happy_var_9
                                  , _time = happy_var_12
                                  , _action = happy_var_15 }
	) `HappyStk` happyRest

happyReduce_76 = happySpecReduce_1  21 happyReduction_76
happyReduction_76 (HappyAbsSyn16  happy_var_1)
	 =  HappyAbsSyn21
		 (fromInteger happy_var_1
	)
happyReduction_76 _  = notHappyAtAll 

happyReduce_77 = happySpecReduce_1  21 happyReduction_77
happyReduction_77 (HappyTerminal (TokenDecimal happy_var_1))
	 =  HappyAbsSyn21
		 (happy_var_1
	)
happyReduction_77 _  = notHappyAtAll 

happyReduce_78 = happyMonadReduce 10 22 happyReduction_78
happyReduction_78 (_ `HappyStk`
	(HappyAbsSyn24  happy_var_9) `HappyStk`
	(HappyAbsSyn23  happy_var_8) `HappyStk`
	(HappyAbsSyn28  happy_var_7) `HappyStk`
	(HappyTerminal (TokenName happy_var_6)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TokenName happy_var_3)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((( do { let r = RoleSpec { _roleSpecName = ""
                                    , _roleRace = happy_var_6
                                    , _roleTeam = happy_var_3
                                    , _table = happy_var_7
                                    , _setupSpec = happy_var_8
                                    , _death = happy_var_9
                                    , _connectedRoles = happy_var_9 }
                 ; validateRole r
                 ; return $ \roleName -> set roleSpecName roleName r }))
	) (\r -> happyReturn (HappyAbsSyn22 r))

happyReduce_79 = happySpecReduce_0  23 happyReduction_79
happyReduction_79  =  HappyAbsSyn23
		 (Set.empty
	)

happyReduce_80 = happySpecReduce_3  23 happyReduction_80
happyReduction_80 (HappyAbsSyn26  happy_var_3)
	_
	_
	 =  HappyAbsSyn23
		 (happy_var_3
	)
happyReduction_80 _ _ _  = notHappyAtAll 

happyReduce_81 = happySpecReduce_0  24 happyReduction_81
happyReduction_81  =  HappyAbsSyn24
		 (Set.empty
	)

happyReduce_82 = happySpecReduce_3  24 happyReduction_82
happyReduction_82 (HappyAbsSyn26  happy_var_3)
	_
	_
	 =  HappyAbsSyn24
		 (happy_var_3
	)
happyReduction_82 _ _ _  = notHappyAtAll 

happyReduce_83 = happySpecReduce_0  25 happyReduction_83
happyReduction_83  =  HappyAbsSyn25
		 (Set.empty
	)

happyReduce_84 = happyReduce 4 25 happyReduction_84
happyReduction_84 ((HappyAbsSyn26  happy_var_4) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn25
		 (happy_var_4
	) `HappyStk` happyRest

happyReduce_85 = happySpecReduce_1  26 happyReduction_85
happyReduction_85 (HappyAbsSyn27  happy_var_1)
	 =  HappyAbsSyn26
		 (Set.fromList happy_var_1
	)
happyReduction_85 _  = notHappyAtAll 

happyReduce_86 = happySpecReduce_0  27 happyReduction_86
happyReduction_86  =  HappyAbsSyn27
		 ([]
	)

happyReduce_87 = happySpecReduce_2  27 happyReduction_87
happyReduction_87 (HappyAbsSyn27  happy_var_2)
	(HappyTerminal (TokenName happy_var_1))
	 =  HappyAbsSyn27
		 (happy_var_1 : happy_var_2
	)
happyReduction_87 _ _  = notHappyAtAll 

happyReduce_88 = happySpecReduce_0  28 happyReduction_88
happyReduction_88  =  HappyAbsSyn28
		 (Map.empty
	)

happyReduce_89 = happySpecReduce_2  28 happyReduction_89
happyReduction_89 (HappyAbsSyn28  happy_var_2)
	(HappyAbsSyn29  happy_var_1)
	 =  HappyAbsSyn28
		 (Map.insert (fst happy_var_1) (snd happy_var_1) happy_var_2
	)
happyReduction_89 _ _  = notHappyAtAll 

happyReduce_90 = happyReduce 5 29 happyReduction_90
happyReduction_90 ((HappyTerminal (TokenName happy_var_5)) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (TokenName happy_var_3)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn29
		 ((happy_var_3, happy_var_5)
	) `HappyStk` happyRest

happyReduce_91 = happyReduce 12 30 happyReduction_91
happyReduction_91 ((HappyAbsSyn27  happy_var_12) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn26  happy_var_9) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn26  happy_var_6) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn31  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn30
		 ((happy_var_3, happy_var_6, happy_var_9, happy_var_12)
	) `HappyStk` happyRest

happyReduce_92 = happySpecReduce_2  31 happyReduction_92
happyReduction_92 (HappyAbsSyn31  happy_var_2)
	(HappyAbsSyn31  happy_var_1)
	 =  HappyAbsSyn31
		 (joinDrive happy_var_1 happy_var_2
	)
happyReduction_92 _ _  = notHappyAtAll 

happyReduce_93 = happySpecReduce_1  32 happyReduction_93
happyReduction_93 _
	 =  HappyAbsSyn31
		 ("/"
	)

happyReduce_94 = happySpecReduce_1  33 happyReduction_94
happyReduction_94 (HappyTerminal (TokenName happy_var_1))
	 =  HappyAbsSyn31
		 (happy_var_1
	)
happyReduction_94 _  = notHappyAtAll 

happyReduce_95 = happySpecReduce_3  33 happyReduction_95
happyReduction_95 (HappyAbsSyn31  happy_var_3)
	_
	(HappyTerminal (TokenName happy_var_1))
	 =  HappyAbsSyn31
		 (happy_var_1 </> happy_var_3
	)
happyReduction_95 _ _ _  = notHappyAtAll 

happyNewToken action sts stk [] =
	action 111 111 notHappyAtAll (HappyState action) sts stk []

happyNewToken action sts stk (tk:tks) =
	let cont i = action i i tk (HappyState action) sts stk tks in
	case tk of {
	TokenHash -> cont 34;
	TokenEq -> cont 35;
	TokenSlash -> cont 36;
	TokenNeq -> cont 37;
	TokenLt -> cont 38;
	TokenGt -> cont 39;
	TokenLeq -> cont 40;
	TokenGeq -> cont 41;
	TokenOpenBracket -> cont 42;
	TokenCloseBracket -> cont 43;
	TokenHyphen -> cont 44;
	TokenColon -> cont 45;
	TokenName "prompt" -> cont 46;
	TokenName "finish" -> cont 47;
	TokenName "order" -> cont 48;
	TokenName "time" -> cont 49;
	TokenName "action" -> cont 50;
	TokenName "extra-center" -> cont 51;
	TokenName "setup" -> cont 52;
	TokenName "death" -> cont 53;
	TokenName "default" -> cont 54;
	TokenName "any" -> cont 55;
	TokenName "died" -> cont 56;
	TokenName "in" -> cont 57;
	TokenName "play" -> cont 58;
	TokenName "most" -> cont 59;
	TokenName "voted" -> cont 60;
	TokenName "not" -> cont 61;
	TokenName "and" -> cont 62;
	TokenName "or" -> cont 63;
	TokenName "for" -> cont 64;
	TokenName "of" -> cont 65;
	TokenName "self" -> cont 66;
	TokenName "left" -> cont 67;
	TokenName "right" -> cont 68;
	TokenName "adjacent" -> cont 69;
	TokenName "others" -> cont 70;
	TokenName "everyone" -> cont 71;
	TokenName "team" -> cont 72;
	TokenName "role" -> cont 73;
	TokenName "choose" -> cont 74;
	TokenName "from" -> cont 75;
	TokenName "then" -> cont 76;
	TokenName "know" -> cont 77;
	TokenName "look" -> cont 78;
	TokenName "at" -> cont 79;
	TokenName "swap" -> cont 80;
	TokenName "with" -> cont 81;
	TokenName "may" -> cont 82;
	TokenName "if" -> cont 83;
	TokenName "chosen" -> cont 84;
	TokenName "center" -> cont 85;
	TokenName "extra" -> cont 86;
	TokenName "union" -> cont 87;
	TokenName "intersect" -> cont 88;
	TokenName "card" -> cont 89;
	TokenName "defaulting" -> cont 90;
	TokenName "to" -> cont 91;
	TokenName "named" -> cont 92;
	TokenName "cards" -> cont 93;
	TokenName "all" -> cont 94;
	TokenName "race" -> cont 95;
	TokenName "last" -> cont 96;
	TokenName "players" -> cont 97;
	TokenName "who" -> cont 98;
	TokenName "directory" -> cont 99;
	TokenName "races" -> cont 100;
	TokenName "teams" -> cont 101;
	TokenName "roles" -> cont 102;
	TokenName "connected" -> cont 103;
	TokenName "awake" -> cont 104;
	TokenInt 1 -> cont 105;
	TokenInt 2 -> cont 106;
	TokenInt 3 -> cont 107;
	TokenInt happy_dollar_dollar -> cont 108;
	TokenDecimal happy_dollar_dollar -> cont 109;
	TokenName happy_dollar_dollar -> cont 110;
	_ -> happyError' ((tk:tks), [])
	}

happyError_ explist 111 tk tks = happyError' (tks, explist)
happyError_ explist _ tk tks = happyError' ((tk:tks), explist)

happyThen :: () => ParserMonad a -> (a -> ParserMonad b) -> ParserMonad b
happyThen = (>>=)
happyReturn :: () => a -> ParserMonad a
happyReturn = (return)
happyThen1 m k tks = (>>=) m (\a -> k a tks)
happyReturn1 :: () => a -> b -> ParserMonad a
happyReturn1 = \a tks -> (return) a
happyError' :: () => ([(Token)], [String]) -> ParserMonad a
happyError' = (\(tokens, _) -> parseError tokens)
parseAction tks = happySomeParser where
 happySomeParser = happyThen (happyParse action_0 tks) (\x -> case x of {HappyAbsSyn10 z -> happyReturn z; _other -> notHappyAtAll })

parseCondition tks = happySomeParser where
 happySomeParser = happyThen (happyParse action_1 tks) (\x -> case x of {HappyAbsSyn13 z -> happyReturn z; _other -> notHappyAtAll })

parsePlayerSet tks = happySomeParser where
 happySomeParser = happyThen (happyParse action_2 tks) (\x -> case x of {HappyAbsSyn11 z -> happyReturn z; _other -> notHappyAtAll })

parseNumber tks = happySomeParser where
 happySomeParser = happyThen (happyParse action_3 tks) (\x -> case x of {HappyAbsSyn17 z -> happyReturn z; _other -> notHappyAtAll })

parseSetup tks = happySomeParser where
 happySomeParser = happyThen (happyParse action_4 tks) (\x -> case x of {HappyAbsSyn20 z -> happyReturn z; _other -> notHappyAtAll })

parseRoleSpec tks = happySomeParser where
 happySomeParser = happyThen (happyParse action_5 tks) (\x -> case x of {HappyAbsSyn22 z -> happyReturn z; _other -> notHappyAtAll })

parseGameFile tks = happySomeParser where
 happySomeParser = happyThen (happyParse action_6 tks) (\x -> case x of {HappyAbsSyn30 z -> happyReturn z; _other -> notHappyAtAll })

happySeq = happyDontSeq


parseError :: [Token] -> ParserMonad a
parseError tokens = throwError $ "Parse error with tokens: " ++ show tokens
  
isValidFirst,isValidRest :: Char -> Bool
isValidFirst = isAlpha
isValidRest char = isAlphaNum char || char `elem` "-+_"

isValidName :: String -> Bool
isValidName "" = False
isValidName (c:cs) = isValidFirst c && all isValidRest cs

lexer :: String -> [Token]
lexer [] = []
lexer ('"':cs) = TokenName name : lexer rest
  where (name, _ : rest) = span (/= '"') cs
lexer ('#':cs) = TokenHash : lexer cs
lexer ('-':cs) = TokenHyphen : lexer cs
lexer (':':cs) = TokenColon : lexer cs
lexer ('=':cs) = TokenEq : lexer cs
lexer ('/':'=':cs) = TokenNeq : lexer cs
lexer ('/':cs) = TokenSlash : lexer cs
lexer ('<':'=':cs) = TokenLeq : lexer cs
lexer ('>':'=':cs) = TokenGeq : lexer cs
lexer ('<':cs) = TokenLt : lexer cs
lexer ('>':cs) = TokenGt : lexer cs
lexer ('(':cs) = TokenOpenBracket : lexer cs
lexer (')':cs) = TokenCloseBracket : lexer cs
lexer (c:cs)
      | isSpace c = lexer cs
      | isDigit c = lexNum (c:cs)
      | otherwise = TokenName (c:name) : lexer rest
      where (name, rest) = span isValidRest cs

lexNum :: String -> [Token]
lexNum cs =
  if isDecimal
  then TokenDecimal decimal : lexer rest
  else TokenInt (read num) : lexer afterNum
      where (num, afterNum) = span isDigit cs
            isDecimal = afterNum /= "" && head afterNum == '.'
            (fractional, rest) = span isDigit $ tail afterNum
            decimal = fst $ head $
                      readFloat $
                      num ++ '.' : fractional

parserToReader :: ParserMonadKinds m => Parser a -> String -> m a
parserToReader parser = view pm . parser . lexer

parserToRead :: Parser a -> Context -> String -> a
parserToRead parser context input =
  case runParser (parserToReader parser input) context of
    Left msg    -> error msg
    Right value -> value
    

parserToReaderFile :: (ParserMonadKinds m, MonadIO m) =>
                      Parser a -> FilePath -> m a
parserToReaderFile parser path =
  (liftIO (readFile path) >>= parserToReader parser) `catchError`
  \msg -> throwError $ path ++ ':' : msg

parserToReadFile :: (MonadIO m) => Parser a -> Context -> FilePath -> m a
parserToReadFile parser context path =
         parserToRead parser context <$> liftIO (readFile path)
{-# LINE 1 "templates/GenericTemplate.hs" #-}
-- $Id: GenericTemplate.hs,v 1.26 2005/01/14 14:47:22 simonmar Exp $










































data Happy_IntList = HappyCons Int Happy_IntList








































infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

-- If the current token is ERROR_TOK, it means we've just accepted a partial
-- parse (a %partial parser).  We must ignore the saved token on the top of
-- the stack in this case.
happyAccept (1) tk st sts (_ `HappyStk` ans `HappyStk` _) =
        happyReturn1 ans
happyAccept j tk st sts (HappyStk ans _) = 
         (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action









































indexShortOffAddr arr off = arr Happy_Data_Array.! off


{-# INLINE happyLt #-}
happyLt x y = (x < y)






readArrayBit arr bit =
    Bits.testBit (indexShortOffAddr arr (bit `div` 16)) (bit `mod` 16)






-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Int ->                    -- token number
         Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = let r = fn v1 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = let r = fn v1 v2 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = let r = fn v1 v2 v3 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happyReduce k i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happyReduce k nt fn j tk st sts stk
     = case happyDrop (k - ((1) :: Int)) sts of
         sts1@(((st1@(HappyState (action))):(_))) ->
                let r = fn stk in  -- it doesn't hurt to always seq here...
                happyDoSeq r (action nt j tk st1 sts1 r)

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
          let drop_stk = happyDropStk k stk in
          happyThen1 (fn stk tk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))

happyMonad2Reduce k nt fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happyMonad2Reduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
         let drop_stk = happyDropStk k stk





             _ = nt :: Int
             new_state = action

          in
          happyThen1 (fn stk tk) (\r -> happyNewToken new_state sts1 (r `HappyStk` drop_stk))

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n - ((1) :: Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n - ((1)::Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction









happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery (ERROR_TOK is the error token)

-- parse error if we are in recovery and we fail again
happyFail explist (1) tk old_st _ stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--      trace "failing" $ 
        happyError_ explist i tk

{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  ERROR_TOK tk old_st CONS(HAPPYSTATE(action),sts) 
                                                (saved_tok `HappyStk` _ `HappyStk` stk) =
--      trace ("discarding state, depth " ++ show (length stk))  $
        DO_ACTION(action,ERROR_TOK,tk,sts,(saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail explist i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
        action (1) (1) tk (HappyState (action)) sts ((HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll :: a
notHappyAtAll = error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Seq-ing.  If the --strict flag is given, then Happy emits 
--      happySeq = happyDoSeq
-- otherwise it emits
--      happySeq = happyDontSeq

happyDoSeq, happyDontSeq :: a -> b -> b
happyDoSeq   a b = a `seq` b
happyDontSeq a b = b

-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.









{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.
