{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
module Data.Lenses where

import Data.Maybe (fromMaybe)

import Control.Lens hiding (Context)

import Data.Map (Map)
import qualified Data.Map as Map

import Data.Core

makeLenses ''Card
makeLenses ''Role
makeLenses ''Context
makeLenses ''RoleSpec
makeLenses ''Setup
makeLenses ''GameContext
makeLenses ''Player
makeLenses ''PlayerState
makeLenses ''GameState
makeLenses ''Global
makeLenses ''InGameView

makePrisms ''PlayerEvent

instance Show Setup where
  show setup = "SetupPhase(name=" ++ setup^.phaseName.to show ++
               ",begin=" ++ setup^.beginPrompt.to show ++
               ",finish=" ++ setup^.finishPrompt.to show ++
               ",order=" ++ setup^.order.to show ++
               ",lasts for=" ++ setup^.time.to show ++ ")"

instance Show GameContext where
  show game = "GameContext(teams=" ++ game^.gameTeams.to Map.keys.to show ++
              ",races=" ++ game^.gameRaces.to show ++
              ",roles=" ++ game^.gameRoles.to show ++
              ",setup=" ++ game^.gameSetup.to show ++
              ",table=" ++ game^.gameTable.to show ++
              ",cards=" ++ game^.gameCards.to show ++ ")"


mapLens :: (Ord k, Show k) => k -> Lens' (Map k a) a
mapLens k = at k.lens (fromMaybe errorValue) (const Just)
  where errorValue = error $ "MapLens error: key " ++ show k ++ " not in map"

player :: Player -> Lens' PlayerData PlayerState
player = mapLens

pos :: CardPos -> Lens' CardLayout Card
pos = mapLens


-- events :: Lens' GameState [Event]
-- events = lens Core.events $ \r new -> r { Core.events = new }

-- players :: Lens' GameState PlayerData
-- players = lens Core.players $ \r new -> r { Core.players = new }

-- pNum :: Int -> Lens' PlayerData PlayerState
-- pNum = player . Player

-- knowledge :: Lens' PlayerState [PlayerEvent]
-- knowledge = lens Core.knowledge $ \r new -> r { Core.knowledge = new }

-- awake :: Lens' PlayerState Bool
-- awake = lens Core.awake $ \r new -> r { Core.awake = new }

-- vote :: Lens' PlayerState (Maybe Player)
-- vote = lens Core.votesFor $ \r new -> r { Core.votesFor = new }

-- role :: Lens' PlayerState RoleRef
-- role = lens Core.initialRole $ \r new -> r { Core.initialRole = new }

-- cards :: Lens' GameState CardLayout
-- cards = lens Core.cards $ \r new -> r { Core.cards = new }


-- faceUp :: Lens' Card Bool
-- faceUp = lens Core.isFaceUp $ \r new -> r { Core.isFaceUp = new }

-- face :: Lens' Card CardFace
-- face = lens Core.nominalRole $ \r new -> r { Core.nominalRole = new }

-- actualRole :: Lens' Card Role
-- actualRole = lens Core.actualRole $ \r new -> r { Core.actualRole = new }

-- setup :: Lens' Role (Set SetupRef)
-- setup = lens Core.setup $ \r new -> r { Core.setup = new }
