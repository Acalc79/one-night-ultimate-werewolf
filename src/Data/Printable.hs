module Data.Printable where

import qualified Data.Foldable as Fold
import qualified Data.List as List

import Data.Core

class Printable a where
  pprint :: a -> String

instance Printable ActionSpec where
  pprint (NamedAction name _) = name
  pprint NoOp = "nothing"
  pprint (ChooseNFrom 0 _ _) = "nothing"
  pprint (ChooseNFrom 1 set def) =
    "choose a card from " ++ pprint set ++ " (default: " ++ pprint def ++ ")"
  pprint (ChooseNFrom n set def) =
    "choose " ++ show n ++ " cards from " ++ pprint set ++
    " (default: " ++ pprint def ++ ")"
  pprint (KnowInPlay set) = "see " ++ pprint set ++ " in play"
  pprint (LookAt set) = "look at " ++ pprint set
  pprint (SwapWith card0 card1) =
    "swap " ++ pprint card0 ++ " with " ++ pprint card1
  pprint (IfThen c a) = "if " ++ pprint c ++ " then do " ++ pprint a
  pprint (ActionOr NoOp a) = "you may " ++ pprint a
  pprint (ActionOr a0 a1) = "either " ++ pprint a0 ++ " or " ++ pprint a1
  pprint (AndThen a0 a1) = pprint a0 ++ " and then " ++ pprint a1
  
instance Printable PlayerSetSpec where
  pprint (FromPlayerSpecs set)
    | length set == 1 = pprint $ head $ Fold.toList set
    | otherwise       = mconcat $
    ["["] ++ List.intersperse ", " (pprint <$> Fold.toList set) ++ ["]"]
  pprint (PlayersSatisfying c) = "players who are " ++ pprint c
  pprint (PSDifference set0 set1) = pprint set0 ++ " but not " ++ pprint set1
  pprint (PSUnion set0 set1) = "both " ++ pprint set0 ++ " and " ++ pprint set1
  pprint (PSIntersection set0 set1) =
    "set intersection of " ++ pprint set0 ++ " and " ++ pprint set1

instance Printable PlayerSpec where
  pprint Self = "self"
  pprint PSLeft = "player to the left"
  pprint PSRight = "player to the right"

instance Printable CardSetSpec where
  pprint (FromCardPosSpecs set)
    | length set == 1 = pprint $ head $ Fold.toList set
    | otherwise       = mconcat $
    ["["] ++ List.intersperse ", " (pprint <$> Fold.toList set) ++ ["]"]
  pprint (OfPlayers set) = "cards of " ++ pprint set
  pprint (LastNChosen n) | n < 1     = "nothing"
                         | n == 1    = pprint LastChosen
                         | otherwise = "last " ++ show n ++ " chosen cards"
  pprint AllCards = "all cards"
  pprint (CSDifference set0 set1) = pprint set0 ++ " but not " ++ pprint set1
  pprint (CSUnion set0 set1) = "both " ++ pprint set0 ++ " and " ++ pprint set1
  pprint (CSIntersection set0 set1) =
    "set intersection of " ++ pprint set0 ++ " and " ++ pprint set1

instance Printable CardPosSpec where
  pprint (GlobalCP card) = show card
  pprint (PlayerRelative player) = "card of " ++ pprint player
  pprint LastChosen = "chosen card"

instance Printable ConditionSpec where
  pprint Awake = "awake"
  pprint Dead = "dead"
  pprint DefaultDeath = "default death condition"
  pprint (AllOf players c) =
    "all of " ++ pprint players ++ " satisfy " ++ pprint c
  pprint (AnyOf players c) =
    "at least one of " ++ pprint players ++ " satisfies " ++ pprint c
  pprint (Numeric n0 op n1) =
    pprint n0 ++ " " ++ pprint op ++ " " ++ pprint n1
  pprint (VotedFor p) = "voted for " ++ pprint p
  pprint MostVoted = "got most votes"
  pprint (RoleNameIs name) = "role " ++ name
  pprint (RoleRaceIs name) = "race " ++ name
  pprint (RoleTeamIs name) = "team " ++ name
  pprint (Not c) = "not " ++ pprint c
  pprint (And c0 c1) = pprint c0 ++ " and " ++ pprint c1
  pprint (Or c0 c1) = pprint c0 ++ " or " ++ pprint c1

instance Printable Op where
  pprint OpEq = "="
  pprint OpNeq = "=/="
  pprint OpLt = "<"
  pprint OpGt = ">"
  pprint OpLeq = "<="
  pprint OpGeq = ">="

instance Printable NumSpec where
  pprint (GlobalN n) = show n
  pprint (SizePlayerSet set) = "number of " ++ pprint set
