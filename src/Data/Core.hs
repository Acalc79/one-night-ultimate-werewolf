{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
module Data.Core where

import Control.Monad.Reader (MonadReader, ask, local)
import Data.Set (Set)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

type TeamRef = FilePath
type RoleRef = FilePath
type DeathRef = FilePath
type SetupRef = FilePath
type Race = String

type CardFace = RoleRef
data Card = Card { _nominalRole :: !CardFace
                 , _actualRole :: !Role
                 , _isFaceUp :: !Bool
                 } deriving (Show, Eq, Ord)

data Role = Role { _roleName :: !RoleRef
                 , _race :: !Race
                 , _team :: !TeamRef
                 , _setup :: !(Set SetupRef)
                 } deriving (Show, Eq, Ord)

data Context = Context { _teams :: [TeamRef]
                       , _roles :: [RoleRef]
                       , _setups :: [SetupRef]
                       , _deaths :: [DeathRef]
                       } deriving (Show, Eq)

emptyContext = Context { _teams = [], _roles = [], _setups = [], _deaths = [] }

data RoleSpec = RoleSpec { _roleSpecName :: RoleRef
                         , _roleRace :: Race
                         , _roleTeam :: TeamRef
                         , _table :: Map String RoleRef
                         , _setupSpec :: Set SetupRef
                         , _death :: Set DeathRef
                         , _connectedRoles :: Set RoleRef
                         } deriving (Show, Eq, Ord)

data Setup = Setup { _phaseName :: !SetupRef
                   , _beginPrompt :: !String
                   , _finishPrompt :: !String
                   , _order :: !Rational
                   , _time :: !Integer
                   , _action :: !ActionSpec
                   }

-- gameRoles field is necessary in addition to gameCards
-- because some roles appear in the game at later stages, e.g. through Doppelganger
data GameContext = GameContext { _gameTeams :: !(Map TeamRef ConditionSpec)
                               , _gameRaces :: !(Set Race)
                               , _gameRoles :: !(Map RoleRef Role)
                               , _gameSetup :: !(Map SetupRef Setup)
                               , _gameTable :: !(Map String RoleRef)
                               , _gameCards :: ![Card]
                               , _defaultDeath :: !ConditionSpec
                               , _gameDeath :: !ConditionSpec
                               }

newtype Player = Player Int deriving (Show, Eq, Ord)
data PlayerState = PS { _playerName :: String
                      , _initialRole :: RoleRef
                      , _dead :: Bool
                      , _awake :: Bool
                      , _votesFor :: Maybe Player
                      , _knowledge :: [PlayerEvent]
                      } deriving (Eq, Show)

data CardPos where
     Center1 :: CardPos
     Center2 :: CardPos
     Center3 :: CardPos
     Extra :: String -> CardPos
     OfPlayer :: Player -> CardPos
     deriving (Show, Eq, Ord)

type PlayerData = Map Player PlayerState
type CardLayout = Map CardPos Card
data GameState = GameState { _initialCards :: CardLayout
                           , _cards :: CardLayout
                           , _players :: PlayerData
                           , _events :: [Event]
                           }

data Event where
  NightPhaseStarts :: SetupRef -> Event
  NightPhaseEnds :: SetupRef -> Event
  PlayerEvent :: Player -> PlayerEvent -> Event
  deriving (Show, Eq, Ord)

data PlayerEvent where
  WakeUp :: PlayerEvent
  GoToSleep :: PlayerEvent
  SeeAnotherAwake :: Player -> PlayerEvent
  SeeAnotherAsleep :: Player -> PlayerEvent
  SeeSignFrom :: Player -> PlayerEvent
  SeeThatCardIs :: CardPos -> CardFace -> PlayerEvent
  SwapCards :: CardPos -> CardPos -> PlayerEvent
  FlipCard :: CardPos -> PlayerEvent
  ChangeRoleOf :: CardPos -> Role -> PlayerEvent
  ChooseCard :: CardPos -> PlayerEvent
  deriving (Show, Eq, Ord)

type Perspective = (GameContext, Player)
type GamePerspective = (GameContext, GameState, Player)
type GameData = (GameContext, GameState)

newtype Global a =
  Global { _unGlobal :: forall m. (MonadReader GameData m) => m a }
  deriving (Functor)

instance Applicative Global where
  pure a = Global $ pure a
  (Global f) <*> (Global a) = Global $ f <*> a

instance Monad Global where
  (Global ma) >>= f = Global $ ma >>= _unGlobal . f

instance MonadReader GameData Global where
  ask = Global ask
  local f (Global m) = Global $ local f m

newtype InGameView a =
  InView { _gameView :: forall m. (MonadReader GamePerspective m) => m a }
  deriving (Functor)

instance Applicative InGameView where
  pure a = InView $ pure a
  (InView f) <*> (InView a) = InView $ f <*> a

instance Monad InGameView where
  (InView ma) >>= f = InView $ ma >>= _gameView . f

instance MonadReader GamePerspective InGameView where
  ask = InView ask
  local f (InView m) = InView $ local f m

data Op = OpEq | OpNeq | OpLt | OpGt | OpLeq | OpGeq deriving (Show, Eq, Ord)

data CardPosSpec where
  GlobalCP :: CardPos -> CardPosSpec
  PlayerRelative :: PlayerSpec -> CardPosSpec
  LastChosen :: CardPosSpec
  deriving (Show, Eq, Ord)

data CardSetSpec where
  FromCardPosSpecs :: Set CardPosSpec -> CardSetSpec
  OfPlayers :: PlayerSetSpec -> CardSetSpec
  LastNChosen :: Integer -> CardSetSpec
  AllCards :: CardSetSpec
  CSDifference :: CardSetSpec -> CardSetSpec -> CardSetSpec
  CSUnion :: CardSetSpec -> CardSetSpec -> CardSetSpec
  CSIntersection :: CardSetSpec -> CardSetSpec -> CardSetSpec
  deriving (Show, Eq, Ord)

data PlayerSpec where
  Self :: PlayerSpec
  PSLeft :: PlayerSpec
  PSRight :: PlayerSpec
  deriving (Show, Eq, Ord)

data PlayerSetSpec where
  FromPlayerSpecs :: Set PlayerSpec -> PlayerSetSpec
  PlayersSatisfying :: ConditionSpec -> PlayerSetSpec
  PSDifference :: PlayerSetSpec -> PlayerSetSpec -> PlayerSetSpec
  PSUnion :: PlayerSetSpec -> PlayerSetSpec -> PlayerSetSpec
  PSIntersection :: PlayerSetSpec -> PlayerSetSpec -> PlayerSetSpec
  deriving (Show, Eq, Ord)

data NumSpec where
  GlobalN :: Integer -> NumSpec
  SizePlayerSet :: PlayerSetSpec -> NumSpec
  deriving (Show, Eq, Ord)

-- data Operator = OpEq | OpNeq | OpLt | OpGt | OpLeq | OpGeq
--   deriving (Show, Eq, Ord)

data ConditionSpec where
  Awake :: ConditionSpec
  Dead :: ConditionSpec
  DefaultDeath :: ConditionSpec
  AllOf :: PlayerSetSpec -> ConditionSpec -> ConditionSpec
  AnyOf :: PlayerSetSpec -> ConditionSpec -> ConditionSpec
  Numeric :: NumSpec -> Op -> NumSpec -> ConditionSpec
  VotedFor :: PlayerSpec -> ConditionSpec
  MostVoted :: ConditionSpec
  RoleNameIs :: String -> ConditionSpec
  RoleRaceIs :: String -> ConditionSpec
  RoleTeamIs :: String -> ConditionSpec
  Not :: ConditionSpec -> ConditionSpec
  And :: ConditionSpec -> ConditionSpec -> ConditionSpec
  Or :: ConditionSpec -> ConditionSpec -> ConditionSpec
  deriving (Show, Eq, Ord)

data ActionSpec where
  NamedAction :: String -> ActionSpec -> ActionSpec
  NoOp :: ActionSpec
  ChooseNFrom :: Integer -> CardSetSpec -> CardSetSpec -> ActionSpec
  KnowInPlay :: PlayerSetSpec -> ActionSpec
  LookAt :: CardSetSpec -> ActionSpec
  SwapWith :: CardPosSpec -> CardPosSpec -> ActionSpec
  IfThen :: ConditionSpec -> ActionSpec -> ActionSpec
  ActionOr :: ActionSpec -> ActionSpec -> ActionSpec
  AndThen :: ActionSpec -> ActionSpec -> ActionSpec
  deriving (Show, Eq, Ord)
