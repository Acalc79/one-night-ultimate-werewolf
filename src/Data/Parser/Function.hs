{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Data.Parser.Function where

import Control.Monad (filterM, when, unless)
import qualified Data.Foldable as Fold
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Maybe (mapMaybe)

import Control.Monad.Reader (MonadReader, runReader, local, ask, asks)
import Control.Monad.Except (MonadError, throwError)

import Control.Lens hiding (Context)

import Data.Core
import Data.Lenses
import Data.Parser.Core

seenEvents :: InGameView [PlayerEvent]
seenEvents = do
  gameState <- view _2
  p <- view _3
  return $ view knowledge $ (gameState^.players) Map.! p

emptyCardSet :: CardSetSpec
emptyCardSet = FromCardPosSpecs Set.empty

fromGlobal :: Global a -> InGameView a
fromGlobal g = asks $ \(ctx, state, _) -> g^.unGlobal $ (ctx, state)

true :: ConditionSpec
true = Or Dead $ Not Dead

everyone :: PlayerSetSpec
everyone = PlayersSatisfying true

asPlayer :: Player -> InGameView a -> InGameView a
asPlayer p = local $ _3 .~ p

validateRole :: forall m.
  (MonadReader Context m, MonadError String m) => RoleSpec -> m ()
validateRole roleSpec = do
  ctx <- ask
  shouldBeAmong "team" [roleSpec^.roleTeam] $ ctx^.teams
  shouldBeAmong "role" (roleSpec^..table.folded) $ ctx^.roles
  shouldBeAmong "setup" (roleSpec^..setupSpec.folded) $ ctx^.setups
  shouldBeAmong "death" (roleSpec^..death.folded) $ ctx^.deaths
  where shouldBeAmong :: String -> [String] -> [String] -> m ()
        shouldBeAmong name tokens reference = do
          let outsiders = filter (`notElem` reference) tokens
          unless (null outsiders) $
            throwError $
              show outsiders ++ " are not known " ++ name ++ " names, " ++
              "known: " ++ show reference
