{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
module Data.Parser.Core where

import Control.Monad.Reader (MonadReader(..), runReaderT)
import Control.Monad.Except (MonadError(..), runExceptT)

import Control.Lens (view, makeLenses)

import Data.Core

data Token = TokenHash
           | TokenSlash
           | TokenEq
           | TokenNeq
           | TokenLt
           | TokenGt
           | TokenLeq
           | TokenGeq
           | TokenOpenBracket
           | TokenCloseBracket
           | TokenHyphen
           | TokenColon
           | TokenInt Integer
           | TokenDecimal Rational
           | TokenName String
           deriving (Show, Eq)

type ErrorType = String
type ParserMonadKinds m = (MonadReader Context m, MonadError ErrorType m)
newtype ParserMonad a = PM { _pm :: forall m. ParserMonadKinds m => m a }
  deriving (Functor)
type Parser a = [Token] -> ParserMonad a

makeLenses ''ParserMonad

instance Applicative ParserMonad where
  pure a = PM $ pure a
  (PM f) <*> (PM a) = PM $ f <*> a

instance Monad ParserMonad where
  (PM ma) >>= f = PM $ ma >>= view pm . f

instance MonadReader Context ParserMonad where
  ask = PM ask
  local f (PM a) = PM $ local f a

instance MonadError ErrorType ParserMonad where
  throwError e = PM $ throwError e
  catchError (PM a) f = PM $ catchError a $ view pm . f

runParser :: ParserMonad a -> Context -> Either ErrorType a
runParser = runReaderT . view pm

runParserT :: (Monad m) => ParserMonad a -> Context -> m (Either ErrorType a)
runParserT (PM ma) = runExceptT . runReaderT ma
