{-# LANGUAGE NumericUnderscores #-}
module Interface where

import Control.Concurrent (threadDelay)
import Control.Monad.IO.Class (MonadIO, liftIO)
import qualified Data.Foldable as Fold
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Maybe (isJust, catMaybes, fromMaybe)
import Text.Read (readMaybe)

import Control.Monad.Except (throwError)
import Control.Concurrent.Async (async, race, wait)
import Data.Time.Clock
  (UTCTime(..), NominalDiffTime
  , getCurrentTime, addUTCTime, diffUTCTime
  , secondsToDiffTime, utctDay)

import Data.Core (Player)
-- import Debug.Trace

getDeadline :: (MonadIO m) => NominalDiffTime -> m UTCTime
getDeadline time = addUTCTime time <$> liftIO getCurrentTime

toMicroseconds :: NominalDiffTime -> Int
toMicroseconds = round . (* 1_000_000)

toDeadlineWithDefault :: UTCTime -> a -> IO a -> IO a
toDeadlineWithDefault deadline def computation =
  fromMaybe def <$> toDeadline deadline computation

toDeadline :: UTCTime -> IO a -> IO (Maybe a)
toDeadline deadline computation = do
   maxTime <- toMicroseconds . diffUTCTime deadline <$> getCurrentTime
   either (const Nothing) Just <$>
     race (threadDelay maxTime) computation

getChoice :: String -> (a -> String) -> Player -> [a] -> IO a
getChoice prompt showElem player choices = do
  choice <- chooseSeveralFrom 1 prompt showElem player choices
  case Fold.toList choice of
    [elem] -> return elem
    result -> throwError $
              userError $
              "Error in implementation of chooseSeveralFrom, " ++
              "instead of one requested element got " ++
              show (showElem <$> result)

chooseSeveralFrom ::
  Int -> String -> (a -> String) -> Player -> [a] -> IO [a]
chooseSeveralFrom num prompt customShow player choices = do
  showTo (Set.singleton player) prompt
  Fold.traverse_ (showTo (Set.singleton player)) $
     zipWith (\a b -> a ++ ": " ++ b) (show <$> [0..]) (customShow <$> choices)
  indices <- getFrom player parser
  return $ (choices !!) <$> Fold.toList indices
  where parser str = if condition
                     then Right choices
                     else Left $ putStrLn $
                                 "Wrong format, give " ++ show num ++
                                 " naturals delimited by spaces"
          where parts = readMaybe <$> words str
                choices = Set.fromList $ catMaybes parts
                condition = all isJust parts && length choices == num

showTo :: Set Player -> String -> IO ()
showTo players message = Fold.for_ players $ \player ->
  putStrLn $ "To " ++ show player ++ ": " ++ message

showToEveryone :: String -> IO ()
showToEveryone = putStrLn

getFrom :: Player -> (String -> Either (IO ()) a) -> IO a
getFrom p parser = do
  line <- getLine
  case parser line of
    Left io     -> io >> getFrom p parser
    Right value -> return value

doWithTimer :: (MonadIO m) => Integer -> (String -> IO ()) -> m a -> m a
doWithTimer seconds showFunction action = do
  id <- liftIO $ async $ timer seconds
  res <- action
  liftIO $ wait id
  return res
  where timer :: Integer -> IO ()
        timer n | n <= 0    = return ()
                | otherwise = do
                    showFunction $ show n
                    threadDelay 1_000_000
                    timer $ n - 1
