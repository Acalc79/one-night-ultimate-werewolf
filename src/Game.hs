{-# LANGUAGE FlexibleContexts #-}
module Game where

import Control.Monad.Reader (MonadReader, ask, runReaderT)
import Control.Monad.State (MonadState, execStateT)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Random (MonadRandom)

import Data.Core (GameContext, GameState)
import Interface (doWithTimer, showToEveryone)

import Game.State (makeInitialState)
import Game.Setup (runSetup)
import Game.Finish (getVotes, deathFixpoint, determineWinners, congratulate)

discussionPhase ::
  (MonadIO m, MonadState GameState m, MonadReader GameContext m) => m ()
discussionPhase =
  doWithTimer 600 showToEveryone $
  liftIO $
  showToEveryone "Discuss to find out what happened"

runOneGame :: ( MonadIO m, MonadRandom m
              , MonadReader (GameContext, [String]) m) => m GameState
runOneGame = do
  (context, playerNames) <- ask
  flip runReaderT context $
    makeInitialState playerNames >>=
    execStateT (do
      runSetup
      discussionPhase
      getVotes 10
      deathFixpoint
      winners <- determineWinners
      congratulate winners)
